<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

Class Student extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
        
    }
    /* Student Class Controller*/
    
   
    public function manage_class()
    {
        $data=array();
        $data['title']='Add Class';
        $data['all_info']=$this->student_model->select_all_class();
        $data['admin_mid_content']=$this->load->view('admin/manage_class',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_class()
    {
        $data=array();
        $data['class_name']=$this->input->post('class_name',true);
        $this->student_model->save_class_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('student/manage_class');
        
    }
    
    public function edit_class($class_id)
    {
        $data=array();
        $data['title']='Edit Class';
        $data['class_info']=$this->student_model->select_class_by_id($class_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_class',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_class()
    {
        $data=array();
        $class_id=$this->input->post('class_id',true);
        $data['class_name']=$this->input->post('class_name',true);
        $this->student_model->update_class($class_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('student/manage_class');
    }
    public function delete_class($class_id)
    {
        $this->student_model->delete_class_by_id($class_id);
        redirect('student/manage_class');
        
    }
    
    /* End Student Class Controller*/
    
    
    
    public function add_student()
    {
        $data=array();
        $data['title']='Add Student';
        $data['all_class']=$this->student_model->select_all_class();
        $data['admin_mid_content']=$this->load->view('admin/add_student',$data,true);
        $this->load->view('admin/admin_master',$data);
                
    }
    
    public function save_student()
    {
        $data=array();
        $data['s_name']=$this->input->post('s_name',true);
        $data['s_year']=$this->input->post('s_year',true);
        $data['class_id']=$this->input->post('class_id',true);
        $data['s_roll']=$this->input->post('s_roll',true);
        $data['f_name']=$this->input->post('f_name',true);
        $data['m_name']=$this->input->post('m_name',true);
        
        $data['s_religion']=$this->input->post('s_religion',true);
        $data['s_sex']=$this->input->post('s_sex',true);
        $data['s_birth_date']=$this->input->post('s_birth_date',true);
        $data['s_contact']=$this->input->post('s_contact',true);
        $data['present_address']=$this->input->post('present_address',true);
        $data['parmanent_address']=$this->input->post('parmanent_address',true);
        
       
        
        /*upload image*/
        
        $config['upload_path'] = 'images/student/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['image']=$config['upload_path'].$fdata['file_name'];
        }
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->student_model->save_student_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('student/add_student');
        
    }
    
   public function manage_student()
   {
       $data=array();
       $data['title']='Manage Student';
       $class_id=$this->input->post('class_id',true);       
       $data['all_student']=$this->student_model->select_student_by_class_id($class_id);
       
       $data['all_class']=$this->student_model->select_all_class();
       $data['admin_mid_content']=$this->load->view('admin/manage_student',$data,true);
       $this->load->view('admin/admin_master',$data);
       
   }
    
   public function edit_student($s_id)
   {
       $data=array();
       $data['title']='Edit Student';
       $data['all_class']=$this->student_model->select_all_class();
       $data['s_info']=$this->student_model->select_student_by_id($s_id);
       $data['admin_mid_content']=$this->load->view('admin/edit_student',$data,true);
       $this->load->view('admin/admin_master',$data);
   }
   
   public function update_student()
   {
        $data=array();
        $s_id=$this->input->post('s_id',true);
        $data['s_name']=$this->input->post('s_name',true);
        $data['s_year']=$this->input->post('s_year',true);
        $data['class_id']=$this->input->post('class_id',true);
        $data['s_roll']=$this->input->post('s_roll',true);
        $data['f_name']=$this->input->post('f_name',true);
        $data['m_name']=$this->input->post('m_name',true);
        
        $data['s_religion']=$this->input->post('s_religion',true);
        $data['s_sex']=$this->input->post('s_sex',true);
        $data['s_birth_date']=$this->input->post('s_birth_date',true);
        $data['s_contact']=$this->input->post('s_contact',true);
        $data['present_address']=$this->input->post('present_address',true);
        $data['parmanent_address']=$this->input->post('parmanent_address',true);
               
        $db_image=$this->student_model->select_student_by_id($s_id);
        
        $config['upload_path']='images/student/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1000';
         //$config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
          
        
         $this->student_model->update_student_info($s_id,$data);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('student/manage_student');
   }
   public function delete_student($s_id)
   {
       $db_image=$this->student_model->select_student_by_id($s_id);
       
       if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
         
         $this->student_model->delete_student_by_id($s_id);
         $sdata=array();
         $sdata['d_message']='Delete Successfully';
         $this->session->set_userdata($sdata);
         redirect('student/manage_student');
        
   }
   
   public function view_student($s_id)
   {
       $data=array();
       $data['s_info']=$this->student_model->select_student_by_id($s_id);      
       $this->load->view('admin/view_profile',$data);
              
      
    }
      
    
}