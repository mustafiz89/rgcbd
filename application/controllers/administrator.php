<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

Class Administrator extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
    }
    public function index()
    {
        $data=array();
        $data['title']='Dashboard';
        $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
      public function logout()
    {
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_id');
        $sdata=array();
        $sdata['message']='You are successfully logout';
        $this->session->set_userdata($sdata);
        redirect('login','refresh');
    }
    
    
    /* Slider INFORMATION */
    
    public function add_slider(){
        
        $data=array();
        $data['title']='Add Slider';
        $data['admin_mid_content']=$this->load->view('admin/add_slider',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_slider()
    {
        $data=array();
        
        $config['upload_path'] = 'images/slider/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1000';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('slider_image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['slider_image']=$config['upload_path'].$fdata['file_name'];
        }
        
        $this->administrator_model->save_slider_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_slider');
        
        
    }
    
    public function manage_slider()
    {
        $data=array();
        $data['title']='Manage Slider';
        $data['all_slider']=$this->administrator_model->select_all_slider();
        $data['admin_mid_content']=$this->load->view('admin/manage_slider',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function delete_slider($slider_id)
    {
        $db_slider_image=$this->administrator_model->select_slider_by_id($slider_id);
        
//        echo'<pre>';
//        print_r($db_slider_image);
//        exit();
        
        if($db_slider_image->slider_image)
         {
             $image=$db_slider_image->slider_image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_slider_by_id($slider_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_slider');
        
        
    }
    
    /*Slider End*/
    
    /* Start welcome message Controller */
    
    public function welcome_message()
    {
        $data=array();
        $data['title']='Welcome Message';
        $data['welcome_info']=$this->administrator_model->select_welcome_message();
        
//       echo '<pre>';
//       print_r($data['welcome_info']);
//       exit;
        $data['admin_mid_content']=$this->load->view('admin/welcome_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function welcome_message_edit($welcome_id)
    {
        $data=array();
        $data['title']='Customize Welcome Message';
        $data['edit_welcome_info']=$this->administrator_model->select_welcome_message_by_id($welcome_id);
//        echo'<pre>';
//        print_r($data['edit_welcome_info']);
//        exit();
         $data['admin_mid_content']=$this->load->view('admin/edit_welcome_message',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_welcome_message()
    {
        $data=array();
        $welcome_id=$this->input->post('welcome_id',true);
        $data['welcome_short_message']=$this->input->post('welcome_short_message',true);
        $data['welcome_long_message']=$this->input->post('welcome_long_message',true);
        $this->administrator_model->update_welcome_message_by_id($data,$welcome_id);
        $sdata=array();
        $sdata['message']='Update Information Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/welcome_message');
    }
    
    /* End welcome message Controller */
    
    /* Collage History Controller*/
    public function collage_history()
    {
        $data=array();
        $data['title']='Collage History';
        $history_id=1;
        $data['history_info']=$this->administrator_model->select_collage_history_by_id($history_id);
        $data['admin_mid_content']=$this->load->view('admin/collage_history',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    
    public function update_collage_history()
    {
        $data=array();
        $history_id=$this->input->post('history_id',true);
        $data['history_title']=$this->input->post('history_title',true);
        $data['history_description']=$this->input->post('history_description',true);
        $this->administrator_model->update_collage_history_by_id($history_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata();
        redirect('administrator/collage_history');
    }
    
    /*End Collage History Controller*/
    /* Principle message Controller */
    
    public function principal_message()
    {
        $data=array();
        $data['title']='Principle Message';
        $data['message_info']=$this->administrator_model->principal_message_info();
        $data['admin_mid_content']=$this->load->view('admin/principal_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function principal_message_edit($message_id)
    {
        $data=array();
        $data['title']='Customize Principal Message';
        $data['message_info']=$this->administrator_model->select_principal_message_by_id($message_id);
//        echo'<pre>';
//        print_r($data['edit_welcome_info']);
//        exit();
         $data['admin_mid_content']=$this->load->view('admin/edit_principal_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_principal_message()
    {
        $data=array();
         $message_id=$this->input->post('message_id',true);
         $data['principal_name']=$this->input->post('principal_name',true); 
         $data['message_short_list']=$this->input->post('message_short_list',true);
         $data['message_long_list']=$this->input->post('message_long_list',true);
        
         $db_image=$this->administrator_model->select_principal_message_by_id($message_id);
          
         
         $config['upload_path']='images/principal/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1000';
         //$config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
          
        
         $this->administrator_model->update_principal_message($data,$message_id);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/principal_message');
            
        
    }
    
    /* End principle message Controller */
    
    /*Citizen Charter Controller */
    
   public function manage_citizen_charter()
   {
       $data=array();
       $data['title']='Citizen Charter';
       $data['charter_info']=$this->administrator_model->select_charter_by_id(1);
       $data['admin_mid_content']=$this->load->view('admin/manage_citizen_charter',$data,true);
       $this->load->view('admin/admin_master',$data);
   }
    
   public function update_citizen_charter()
   {
       $data=array();
       $data['description']=$this->input->post('description',true);
       $charter_id=$this->input->post('charter_id',true);
       $this->administrator_model->update_citizen_charter($charter_id,$data);
       $sdata=array();
       $sdata['message']='Update Successfully';
       $this->session->set_userdata($sdata);
       redirect('administrator/manage_citizen_charter');
   }
    
    
   /*Citizen Charter Controller End */
    
    /*Start techer Controller*/
    
    
    public function add_teacher()
    {
        $data=array();
        $data['title']="Add Teacher's Info";
        $data['admin_mid_content']=$this->load->view('admin/add_teacher',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function save_teacher()
    {
        $data=array();
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['subject']=$this->input->post('subject',true);
        $data['join_date']=$this->input->post('join_date',true);
        $data['contact']=$this->input->post('contact',true);
        $data['email']=$this->input->post('email',true);
        $data['blood_group']=$this->input->post('blood_group',true);
        
        /*upload image */
        
        $config['upload_path'] = 'images/teacher/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['image']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->save_teacher_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_teacher');
        
    }
    
    public function manage_teacher()
    {
        $data=array();
        $data['title']='Manage Information';
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'administrator/manage_teacher';
        
        $config['total_rows'] = $this->db->count_all('tbl_teacher');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '&lt; Prev';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $data['all_teacher_info']=$this->administrator_model->select_all_teacher_info($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/manage_teacher',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_teacher($id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_teacher_info']=$this->administrator_model->select_teacher_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_teacher',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function update_teacher()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['subject']=$this->input->post('subject',true);
        $data['join_date']=$this->input->post('join_date',true);
        $data['contact']=$this->input->post('contact',true);
        $data['email']=$this->input->post('email',true);
        $data['blood_group']=$this->input->post('blood_group',true);
        
        $db_image=$this->administrator_model->select_teacher_by_id($id);
//          echo '<pre>';
//          print_r($db_image);
//          exit;
         
         $config['upload_path']='images/teacher/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1200';
         //$config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        

         $this->administrator_model->update_teacher($id,$data);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/manage_teacher');
         
    }
    public function delete_teacher($id)
    {
        $db_image=$this->administrator_model->select_teacher_by_id($id);
        
        if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_teacher_by_id($id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_teacher');
    }
    
    /*End techer Controller*/
    
    
    
     
    /*Start 3rd staff Controller*/
    public function add_3rd_staff()
    {
        $data=array();
        $data['title']="Add Staff's Info";
        $data['admin_mid_content']=$this->load->view('admin/add_3rd_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_3rd_staff()
    {
        $data=array();
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['contact']=$this->input->post('contact',true);
        
        
        /*upload image */
        
        $config['upload_path'] = 'images/3rd_staff/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['image']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->save_3rd_staff_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_3rd_staff');
    }
    public function manage_3rd_staff()
    {
         $data=array();
        $data['title']='Manage Information';
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'administrator/manage_3rd_staff';
        
        $config['total_rows'] = $this->db->count_all('tbl_3rd_staff');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '&lt; Prev';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $data['all_staff_info']=$this->administrator_model->select_all_3rd_staff_info($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/manage_3rd_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_3rd_staff($id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_info']=$this->administrator_model->select_3rd_staff_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_3rd_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function update_3rd_staff()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['contact']=$this->input->post('contact',true);
        
        
        $db_image=$this->administrator_model->select_3rd_staff_by_id($id);
//          echo '<pre>';
//          print_r($db_image);
//          exit;
         
         $config['upload_path']='images/3rd_staff/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1200';
         $config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
//          echo '<pre>';
//          print_r($data);
//          exit;
        
         $this->administrator_model->update_3rd_staff($id,$data);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/manage_3rd_staff');
    }
    public function delete_3rd_staff($id)
    {
        $db_image=$this->administrator_model->select_3rd_staff_by_id($id);
        
        if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_3rd_staff_by_id($id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_3rd_staff');
    } 
    
    
    /*End 3rd staff Controller*/  
    
     /*Start 4th staff Controller*/
     public function add_4th_staff()
    {
        $data=array();
        $data['title']="Add Staff's Info";
        $data['admin_mid_content']=$this->load->view('admin/add_4th_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
     public function save_4th_staff()
    {
        $data=array();
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['contact']=$this->input->post('contact',true);
        
        
        /*upload image */
        
        $config['upload_path'] = 'images/4th_staff/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['image']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->save_4th_staff_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_4th_staff');
    }
    public function manage_4th_staff()
    {
        $data=array();
        $data['title']='Manage Information';
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'administrator/manage_4th_staff';
        
        $config['total_rows'] = $this->db->count_all('tbl_4th_staff');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '&lt; Prev';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $data['all_staff_info']=$this->administrator_model->select_all_4th_staff_info($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/manage_4th_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
     public function edit_4th_staff($id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_info']=$this->administrator_model->select_4th_staff_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_4th_staff',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_4th_staff()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['name']=$this->input->post('name',true);
        $data['title']=$this->input->post('title',true);
        $data['contact']=$this->input->post('contact',true);
        
        
        $db_image=$this->administrator_model->select_4th_staff_by_id($id);
//          echo '<pre>';
//          print_r($db_image);
//          exit;
         
         $config['upload_path']='images/4th_staff/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1200';
         $config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
//          echo '<pre>';
//          print_r($data);
//          exit;
        
         $this->administrator_model->update_4th_staff($id,$data);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/manage_4th_staff');
    }
    
    public function delete_4th_staff($id)
    {
        $db_image=$this->administrator_model->select_4th_staff_by_id($id);
        
        if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_4th_staff_by_id($id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_4th_staff');
    } 
    
     /*End 4th staff Controller*/
    
    
    
    
    /* Start Notice */
    
    public function add_notice()
    {
        $data=array();
        $data['title']='Add Notice';
        $data['admin_mid_content']=$this->load->view('admin/add_notice',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    
    public function save_notice()
    {
        $data=array();
        $data['notice_title']=$this->input->post('notice_title',true);
        $data['notice_short_description']=$this->input->post('notice_short_description',true);
        $data['notice_long_description']=$this->input->post('notice_long_description',true);
        $this->administrator_model->save_notice_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_notice');
    }
    
    
    public function manage_notice()
    {
        $data=array();
        $data['title']='Mange Information';
        $data['all_info']=$this->administrator_model->select_all_notice();
        $data['admin_mid_content']=$this->load->view('admin/manage_notice',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_notice($notice_id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_info']=$this->administrator_model->select_notice_by_id($notice_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_notice',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_notice()
    {
        $data=array();
        $notice_id=$this->input->post('notice_id',true);
        $data['notice_title']=$this->input->post('notice_title',true);
        $data['notice_short_description']=$this->input->post('notice_short_description',true);
        $data['notice_long_description']=$this->input->post('notice_long_description',true);
        $this->administrator_model->update_notice_info($notice_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_notice');
    }
    
    public function delete_notice($notice_id)
    {
        $this->administrator_model->delete_notice_by_id($notice_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_notice');
        
            
    
    }
    /*End Notice Controller*/
    
    
    /* News And Events Controller*/
    
    public function add_news()
    {
        $data=array();
        $data['title']='Add News';
        $data['admin_mid_content']=$this->load->view('admin/add_news',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    
    public function save_news()
    {
        $data=array();
        $data['news_title']=$this->input->post('news_title',true);
        $data['news_short_description']=$this->input->post('news_short_description',true);
        $data['news_long_description']=$this->input->post('news_long_description',true);
        $this->administrator_model->save_news_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_news');
    }
    
    
    public function manage_news()
    {
        $data=array();
        $data['title']='Mange Information';
        $data['all_info']=$this->administrator_model->select_all_news();
        $data['admin_mid_content']=$this->load->view('admin/manage_news',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_news($news_id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_info']=$this->administrator_model->select_news_by_id($news_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_news',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_news()
    {
        $data=array();
        $news_id=$this->input->post('news_id',true);
        $data['news_title']=$this->input->post('news_title',true);
        $data['news_short_description']=$this->input->post('news_short_description',true);
        $data['news_long_description']=$this->input->post('news_long_description',true);
        $this->administrator_model->update_news_info($news_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_news');
    }
    
    public function delete_news($news_id)
    {
        $this->administrator_model->delete_news_by_id($news_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_news');
        
            
    
    }
    
    
    /* End News And Events Controller*/
    
    /* Start Photo gallary*/
    
    public function add_photo_gallary()
    {
        $data=array();
        $data['title']='Add News';
        $data['admin_mid_content']=$this->load->view('admin/add_photo_gallary',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_photo_gallary()
    {
        $data=array();
        
        
        /*upload image */
        
        $config['upload_path'] = 'images/photo_gallary/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['image']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->save_photo_gallary($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_photo_gallary');
    }
    
    public function manage_photo_gallary()
    {
        $data=array();
        $data['title']='Manage Gallary';
        $data['all_photo']=$this->administrator_model->select_all_photo();
        $data['admin_mid_content']=$this->load->view('admin/manage_photo_gallary',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function delete_photo_gallary($image_id)
    {
        $db_image=$this->administrator_model->select_photo_by_id($image_id);
        
        if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_photo_gallary_by_id($image_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_photo_gallary');
    }
    /* End Photo Gallary */
    
    
    /*Academic Calender Controller*/
    
    
   
    
    public function academic_calender()
    {
        $data=array();
        $data['title']='Academic Calender';
        $data['all_info']=$this->administrator_model->select_all_calender_info();
        $data['admin_mid_content']=$this->load->view('admin/academic_calender',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
     public function save_academic_calender()
    {
        $data=array();
        $data['calender_description']=$this->input->post('calender_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/academic_calender/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('calender_image'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['calender_image']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->save_academic_calender($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/academic_calender');
    }
    
    
    public function delete_calender($calender_id)
    {
        $db_image=$this->administrator_model->select_calender_image_by_id($calender_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->calender_image)
         {
             $image=$db_image->calender_image;
             unlink($image);        
         }
        
        $this->administrator_model->delete_calender_image_by_id($calender_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/academic_calender');
    }
    
    /*End Academic Calender Controller*/
    
    public function add_student()
    {
        $data=array();
        $data['title']='Add Student';
        $data['admin_mid_content']=$this->load->view('admin/add_student',$data,true);
        $this->load->view('admin/admin_master',$data);
                
    }
    
    
}
