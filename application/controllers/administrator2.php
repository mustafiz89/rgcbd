<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

Class Administrator2 extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
    }
    
    /*Start alternate teacher controller*/
    
    public function alternate_teacher()
    {
        
        $data=array();
        $data['title']='Alternate Teacher';
        $data['all_info']=$this->administrator2_model->select_all_teacher_info();
        $data['admin_mid_content']=$this->load->view('admin/alternate_teacher',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
     public function save_alternate_teacher()
    {
        $data=array();
        $data['teacher_description']=$this->input->post('teacher_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/alternate_teacher/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('teacher_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['teacher_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_alternate_teacher($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/alternate_teacher');
    }
    
    
    public function delete_teacher($teacher_id)
    {
        $db_image=$this->administrator2_model->select_teacher_file_by_id($teacher_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->teacher_file)
         {
             $image=$db_image->teacher_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_teacher_file_by_id($teacher_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/alternate_teacher');
    }
    
    
    
    /*End alternate teacher controller*/
    
    /* Admission Information  controller */
    
    public function admission_info()
    {
        $data=array();
        $data['title']='Admission Information';
        $data['all_info']=$this->administrator2_model->select_all_admission_info();
        $data['admin_mid_content']=$this->load->view('admin/admission_info',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_admission_info()
    {
       $data=array();
        $data['admission_description']=$this->input->post('admission_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/admission_info/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('admission_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['admission_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_admission_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/admission_info');
        
                
    }
    
      
    public function delete_admission_info($admission_id)
    {
        
        $db_image=$this->administrator2_model->select_admission_info_by_id($admission_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->admission_file)
         {
             $image=$db_image->admission_file;
             unlink($image);        
         }
        $this->administrator2_model->delete_admission_info_by_id($admission_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/admission_info');
    }
    
    /* End Admission Information  controller */
    
    
    /*admission form controller*/
    
    public function admission_form()
    {
        $data=array();
        $data['title']=' Admission Form';
        $data['all_info']=$this->administrator2_model->select_all_form_info();
        $data['admin_mid_content']=$this->load->view('admin/admission_form',$data,true);
        $this->load->view('admin/admin_master',$data);
    
    }
    
    public function save_admission_form()
    {
        $data=array();
        $data['admission_description']=$this->input->post('admission_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/admission_form/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('admission_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['admission_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_admission_form_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/admission_form');
    }
    
     public function delete_admission_form($admission_id)
    {
        $db_image=$this->administrator2_model->select_admission_form_by_id($admission_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->admission_file)
         {
             $image=$db_image->admission_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_admission_form_by_id($admission_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/admission_form');
    }
    /*End admission form controller*/
    
    
    /* Post Created Controller*/
    
    public function post_created()
    {
        $data=array();
        $data['title']=' Created Post';
        $data['all_info']=$this->administrator2_model->select_all_post_info();
        $data['admin_mid_content']=$this->load->view('admin/post_created',$data,true);
        $this->load->view('admin/admin_master',$data);
    
    }
    public function save_post_created()
    {
        $data=array();
        $data['post_description']=$this->input->post('post_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/post/created/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('post_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['post_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_post_created_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/post_created');
    }  
    
    public function delete_post_created($post_id)
    {
        $db_image=$this->administrator2_model->select_post_created_by_id($post_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->post_file)
         {
             $image=$db_image->post_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_post_created_by_id($post_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/post_created');
    }
    
    
     /* End  Post Created Controller*/
    
    
     /* Post vacant Controller*/
    
    public function post_vacant()
    {
        $data=array();
        $data['title']=' Vacant Post';
        $data['all_info']=$this->administrator2_model->select_all_post_vacant_info();
        $data['admin_mid_content']=$this->load->view('admin/post_vacant',$data,true);
        $this->load->view('admin/admin_master',$data);
    
    }
    public function save_post_vacant()
    {
        $data=array();
        $data['post_description']=$this->input->post('post_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/post/vacant/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('post_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['post_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_post_vacant_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/post_vacant');
    }  
    
    public function delete_post_vacant($post_id)
    {
        $db_image=$this->administrator2_model->select_post_vacant_by_id($post_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->post_file)
         {
             $image=$db_image->post_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_post_vacant_by_id($post_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/post_vacant');
    }
    /* End Post vacant Controller*/
    
    /* Result controller*/
    
    public function result()
    {
        $data=array();
        $data['title']=' Result';
        $data['all_info']=$this->administrator2_model->select_all_result_info();
        $data['admin_mid_content']=$this->load->view('admin/result',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_result()
    {
        $data=array();
        $data['result_description']=$this->input->post('result_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/result/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('result_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['result_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_result_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/result');
    }
    
    public function delete_result($result_id)
    {
        $db_image=$this->administrator2_model->select_result_by_id($result_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->result_file)
         {
             $image=$db_image->result_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_result_by_id($result_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/result');
    }
    /*End result controller*/
    
    /*Class Routine controller*/
    public function class_routine()
    {
        $data=array();
        $data['title']=' Class Routine';
        $data['all_info']=$this->administrator2_model->select_all_routine_info();
        $data['admin_mid_content']=$this->load->view('admin/class_routine',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_class_routine()
    {
        $data=array();
        $data['routine_description']=$this->input->post('routine_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/class_routine/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('routine_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['routine_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_class_routine_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/class_routine');
    }
    
    public function delete_class_routine($routine_id)
    {
        $db_image=$this->administrator2_model->select_routine_by_id($routine_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->routine_file)
         {
             $image=$db_image->routine_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_routine_by_id($routine_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/class_routine');
    }
    
     /*End Class Routine controller*/
        
     /*Exam Routine controller*/
    
    public function exam_routine()
    {
        $data=array();
        $data['title']='Exam Routine';
        $data['all_info']=$this->administrator2_model->select_all_exam_routine_info();
        $data['admin_mid_content']=$this->load->view('admin/exam_routine',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_exam_routine()
    {
        $data=array();
        $data['routine_description']=$this->input->post('routine_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/exam_routine/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('routine_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['routine_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_exam_routine_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/exam_routine');
    }
    
    public function delete_exam_routine($routine_id)
    {
        $db_image=$this->administrator2_model->select_exam_routine_by_id($routine_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->routine_file)
         {
             $image=$db_image->routine_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_exam_routine_by_id($routine_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/exam_routine');
    }
    
    /*End Exam Routine controller*/
    
    /*Exam seat controller*/
    public function exam_seat()
    {
        $data=array();
        $data['title']='Exam Seat';
        $data['all_info']=$this->administrator2_model->select_all_exam_seat_info();
        $data['admin_mid_content']=$this->load->view('admin/exam_seat',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_exam_seat()
    {
        $data=array();
        $data['exam_description']=$this->input->post('exam_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/exam_seat/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('exam_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['exam_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_exam_seat_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/exam_seat');
    }
    
    public function delete_exam_seat($exam_id)
    {
        $db_image=$this->administrator2_model->select_exam_seat_by_id($exam_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->exam_file)
         {
             $image=$db_image->exam_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_exam_seat_by_id($exam_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/exam_seat');
    }
    
    /*End Exam seat controller*/
    
    /*Syllabus controller*/
     public function syllabus()
    {
        $data=array();
        $data['title']='Syllabus';
        $data['all_info']=$this->administrator2_model->select_all_syllabus_info();
        $data['admin_mid_content']=$this->load->view('admin/syllabus',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_syllabus()
    {
        $data=array();
        $data['syllabus_description']=$this->input->post('syllabus_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/syllabus/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('syllabus_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['syllabus_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_syllabus_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/syllabus');
    }
    
    public function delete_syllabus($syllabus_id)
    {
        $db_image=$this->administrator2_model->select_syllabus_by_id($syllabus_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->syllabus_file)
         {
             $image=$db_image->syllabus_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_syllabus_by_id($syllabus_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/syllabus');
    }
     /*End Syllabus controller*/
       
     /* Book list Controller*/
    
    public function book_list()
    {
        $data=array();
        $data['title']='Book List';
        $data['all_info']=$this->administrator2_model->select_all_book_info();
        $data['admin_mid_content']=$this->load->view('admin/book_list',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_book_list()
    {
        $data=array();
        $data['book_description']=$this->input->post('book_description',true);
        
        /*upload file */
        
        $config['upload_path'] = 'file/book_list/';
        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['max_size'] = '0';
        //$config['encrypt_name']  = true;
        $config['remove_spaces']=true;
        //$config['max_width'] ='0';
        //$config['max_height'] ='0';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload',$config);
        
        if(!$this->upload->do_upload('book_file'))
        {
            $error=$this->upload->display_errors();
        }
        else
        {
            $fdata=$this->upload->data();
            $data['book_file']=$config['upload_path'].$fdata['file_name'];
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator2_model->save_book_info($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/book_list');
    }
    
    public function delete_book_list($book_id)
    {
        $db_image=$this->administrator2_model->select_book_by_id($book_id);
//        echo'<pre>';
//        print_r($db_image);
//        exit;
        if($db_image->book_file)
         {
             $image=$db_image->book_file;
             unlink($image);        
         }
        
        $this->administrator2_model->delete_book_by_id($book_id);
        $sdata=array();
        $sdata['d_message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator2/book_list');
    }
    
    /* End Book list Controller*/
}