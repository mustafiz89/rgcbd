<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Administrator_Model extends CI_Model{
    
    
    /*slider Info */
    
    
    public function save_slider_info($data)
    {
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->db->insert('tbl_slider',$data);
    }
    
    public function select_all_slider()
    {
        $this->db->select('*');
        $this->db->from('tbl_slider');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    public function select_slider_by_id($slider_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_slider');
        $this->db->where('slider_id',$slider_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function delete_slider_by_id($slider_id)
    {
        $this->db->where('slider_id',$slider_id);
        $this->db->delete('tbl_slider');
    }
    
    /*  welcome message Model*/
    
    public function select_welcome_message()
    {
        $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        //$this->db->where('welcome_id',1);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_welcome_message_by_id($welcome_id)
    {
         $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
                
    }
    
    public function update_welcome_message_by_id($data,$welcome_id)
    {
        $this->db->where('welcome_id',$welcome_id);
        $this->db->update('tbl_welcome_message',$data);
        
        
    }
    /* End welcome message Model */
    
    /* Collage HIstory Model*/
    
    public function select_collage_history_by_id($history_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_collage_history');
        $this->db->where('history_id',$history_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
    public function update_collage_history_by_id($history_id,$data)
    {
        $this->db->where('history_id',$history_id);
        $this->db->update('tbl_collage_history',$data);
    }
    
    
      /* Collage HIstory Model End*/
    
    /* Principle message Model */
    
    public function principal_message_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_principal_message');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
     public function select_principal_message_by_id($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_principal_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
    public function update_principal_message($data,$message_id)
    {
        $this->db->where('message_id',$message_id);
        $this->db->update('tbl_principal_message',$data);
    }
    /* End Principle message Model */
    
    /* Governing body  Model */
    
    
    public function select_charter_by_id($charter_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_citizen_charter');
        $this->db->where('charter_id',$charter_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_citizen_charter($charter_id,$data)
    {
        $this->db->where('charter_id',$charter_id);
        $this->db->update('tbl_citizen_charter',$data);
    }
    
    
    
     /* End Governing body  Model */
    
 
    /* Teacher's   Model */
     public function save_teacher_info($data)
    {
        $this->db->insert('tbl_teacher',$data);
    }
    
    public function select_all_teacher_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_teacher');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_teacher_by_id($id)
    {
        
        $this->db->select('*');
        $this->db->from('tbl_teacher');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_teacher($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_teacher',$data);
    }
    public function delete_teacher_by_id($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_teacher');
    }
    
    
      /* End Teacher's   Model */ 
    
    
    
    
    
    /* 3rd Staff Model*/
    public function save_3rd_staff_info($data)
    {
        $this->db->insert('tbl_3rd_staff',$data);
    }
    
    public function select_all_3rd_staff_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_3rd_staff');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_3rd_staff_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_3rd_staff');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_3rd_staff($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_3rd_staff',$data);
    }
    public function delete_3rd_staff_by_id($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_3rd_staff');
    }
    
    
     /* End 3rd Staff Model*/
    
    /*4th Staff Model*/
    public function select_all_4th_staff_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_4th_staff');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
     public function save_4th_staff_info($data)
    {
        $this->db->insert('tbl_4th_staff',$data);
    }
    
     public function select_4th_staff_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_4th_staff');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_4th_staff($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_4th_staff',$data);
    }
     public function delete_4th_staff_by_id($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_4th_staff');
    }
    /*End 4th Staff Model*/
    
    /* Notice */
    
    public function save_notice_info($data)
    {
        $this->db->insert('tbl_notice',$data);
    }
    
    public function select_all_notice()
    {
      $this->db->select('*');
      $this->db->from('tbl_notice');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_notice_by_id($notice_id)
    {
       $this->db->select('*');
      $this->db->from('tbl_notice');
      $this->db->where('notice_id',$notice_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    public function update_notice_info($notice_id,$data){
        $this->db->where('notice_id',$notice_id);
        $this->db->update('tbl_notice',$data);
    }
    
    public function delete_notice_by_id($notice_id)
    {
       $this->db->where('notice_id',$notice_id);
        $this->db->delete('tbl_notice');
    }
    /*End Notice Model*/
    
    
    
    /* News and Events Model*/
    
    
    public function save_news_info($data)
    {
        $this->db->insert('tbl_news',$data);
    }
    
    public function select_all_news()
    {
      $this->db->select('*');
      $this->db->from('tbl_news');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_news_by_id($news_id)
    {
       $this->db->select('*');
      $this->db->from('tbl_news');
      $this->db->where('news_id',$news_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    public function update_news_info($news_id,$data){
        $this->db->where('news_id',$news_id);
        $this->db->update('tbl_news',$data);
    }
    
    public function delete_news_by_id($news_id)
    {
       $this->db->where('news_id',$news_id);
        $this->db->delete('tbl_news');
    }
    
    
    
     /* End News and Events Model*/
    
    
    /* Photo Gallary  Model*/

    public function save_photo_gallary($data)
    {
         $this->db->insert('tbl_photo_gallary',$data);
    }
    
    public function select_all_photo()
    {
      $this->db->select('*');
      $this->db->from('tbl_photo_gallary');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_photo_by_id($image_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_photo_gallary');
      $this->db->where('image_id',$image_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    public function delete_photo_gallary_by_id($image_id)
    {
        $this->db->where('image_id',$image_id);
        $this->db->delete('tbl_photo_gallary');
    }
    
    /* End Photo Gallary  Model*/
    /* Academic calender Controller*/
    
    public function select_all_calender_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_academic_calender');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_academic_calender($data)
    {
        $this->db->insert('tbl_academic_calender',$data);
    }
    
    public function select_calender_image_by_id($calender_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_academic_calender');
      $this->db->where('calender_id',$calender_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_calender_image_by_id($calender_id)
    {
        $this->db->where('calender_id',$calender_id);
        $this->db->delete('tbl_academic_calender');
    }
    
    /*End Academic Calender Controller*/
   
}
  
?>