<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class Welcome_model extends CI_Model{
    
    public function select_all_slider()
    {
        $this->db->select('*');
        $this->db->from('tbl_slider');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    public function select_welcome_message($welcome_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_welcome_message');
      $this->db->where('welcome_id',$welcome_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function select_collage_history_by_id($history_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_collage_history');
        $this->db->where('history_id',$history_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    public function select_principal_message_by_id($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_principal_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
   public function select_charter_by_id($charter_id)
   {
        $this->db->select('*');
        $this->db->from('tbl_citizen_charter');
        $this->db->where('charter_id',$charter_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result; 
   }
    
    public function select_all_teacher_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_teacher');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_3rd_staff_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_3rd_staff');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_all_4th_staff_info($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_4th_staff');
       $this->db->limit($per_page,$offset);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_all_photo()
    {
      $this->db->select('*');
      $this->db->from('tbl_photo_gallary');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_all_result()
    {
      $this->db->select('*');
      $this->db->from('tbl_result');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
   
    public function select_all_class_routine()
    {
      $this->db->select('*');
      $this->db->from('tbl_class_routine');
      $this->db->limit(7);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_all_exam_routine()
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_routine');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_syllabus()
    {
      $this->db->select('*');
      $this->db->from('tbl_syllabus');
      $this->db->limit(7);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_book_list()
    {
      $this->db->select('*');
      $this->db->from('tbl_book_list');
      $this->db->limit(7);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_exam_seat()
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_seat');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    /*notice model*/
    
    public function select_all_notice($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_notice');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_notice_by_id($notice_id)
    {
       $this->db->select('*');
      $this->db->from('tbl_notice');
      $this->db->where('notice_id',$notice_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function select_notice_for_right_side_bar()
    {
      $this->db->select('*');
      $this->db->from('tbl_notice');
      $this->db->limit(7);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    /* end notice model*/
    
    /* News and Events Model */
    public function select_all_news($per_page,$offset)
    {
        if($offset==null)
       {
           $offset=0;
       }
      $this->db->select('*');
      $this->db->from('tbl_news');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_news_by_id($news_id)
    {
       $this->db->select('*');
      $this->db->from('tbl_news');
      $this->db->where('news_id',$news_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function select_news_for_home()
    {
      $this->db->select('*');
      $this->db->from('tbl_news');
      $this->db->limit(7);
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    
    /* End News and Events Model */
    
    public function select_calender_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_academic_calender');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_alternate_teacher_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_alternate_teacher');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_admission_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_information');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_all_admission_form_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_form');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_all_class()
    {
      $this->db->select('*');
      $this->db->from('tbl_class');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_student_by_roll($class_id,$s_roll)
    {
      $this->db->select('*');
      $this->db->from('tbl_student');      
      $this->db->join('tbl_class','tbl_student.class_id=tbl_class.class_id');
      $this->db->where('tbl_class.class_id',$class_id);
      $this->db->where('s_roll',$s_roll);
      
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
}
?>