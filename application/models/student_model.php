<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Student_model extends CI_Model{
    
   /* Student class Model*/ 
    
    public function select_all_class()
    {
      $this->db->select('*');
      $this->db->from('tbl_class');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_class_info($data)
    {
        $this->db->insert('tbl_class',$data);
    }
    
    public function select_class_by_id($class_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_class');
      $this->db->where('class_id',$class_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function update_class($class_id,$data)
    {
        $this->db->where('class_id',$class_id);
        $this->db->update('tbl_class',$data);
    }
    public function delete_class_by_id($class_id)
    {
        $this->db->where('class_id',$class_id);
        $this->db->delete('tbl_class');
    }
  /* End Student class Model*/ 
    
     /* Student  Model*/
    
    public function save_student_info($data)
    {
        $this->db->insert('tbl_student',$data);
    }
    
    public function select_student_by_class_id($class_id)
    {
        
      $this->db->select('*');
      $this->db->from('tbl_student');
      $this->db->join('tbl_class','tbl_student.class_id=tbl_class.class_id');
      $this->db->where('tbl_class.class_id',$class_id);
      $this->db->order_by('s_roll','asec');      
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
      
    }
    
    public function select_student_by_id($s_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_student');
      $this->db->join('tbl_class','tbl_class.class_id=tbl_student.class_id');      
      $this->db->where('s_id',$s_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function update_student_info($s_id,$data)
    {
        $this->db->where('s_id',$s_id);
        $this->db->update('tbl_student',$data);
    }
   
    public function delete_student_by_id($s_id)
    {
        $this->db->where('s_id',$s_id);
        $this->db->delete('tbl_student');
    }
    
     /* End Student Model*/
    
    
}
?>