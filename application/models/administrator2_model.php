<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Administrator2_model extends CI_Model{
    
    
    /* Alternate Teacher Model*/
    public function select_all_teacher_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_alternate_teacher');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_alternate_teacher($data)
    {
        $this->db->insert('tbl_alternate_teacher',$data);
    }
    
    public function select_teacher_file_by_id($teacher_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_alternate_teacher');
      $this->db->where('teacher_id',$teacher_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_teacher_file_by_id($teacher_id)
    {
        $this->db->where('teacher_id',$teacher_id);
        $this->db->delete('tbl_alternate_teacher');
    }
    
    /* End Alternate Teacher Model*/
    
    
     /* Admission information Model*/
    
    public function select_all_admission_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_information');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_admission_info($data)
    {
        $this->db->insert('tbl_admission_information',$data);
    }
    
    public function select_admission_info_by_id($admission_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_information');
      $this->db->where('admission_id',$admission_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    
    
    public function delete_admission_info_by_id($admission_id)
    {
         $this->db->where('admission_id',$admission_id);
        $this->db->delete('tbl_admission_information');
    }
    
     /* End Admission Information Model*/
    
    /*  Admission form Model*/
    
    public function select_all_form_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_form');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_admission_form_info($data)
    {
        $this->db->insert('tbl_admission_form',$data);
    }
    
    public function select_admission_form_by_id($admission_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_admission_form');
      $this->db->where('admission_id',$admission_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_admission_form_by_id($admission_id)
    {
        $this->db->where('admission_id',$admission_id);
        $this->db->delete('tbl_admission_form');
    }
    
    /* End Admission form Model*/
       
    /* Post created Model*/
    
    public function select_all_post_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_post_created');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_post_created_info($data)
    {
        $this->db->insert('tbl_post_created',$data);
    }
    
    public function select_post_created_by_id($post_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_post_created');
      $this->db->where('post_id',$post_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_post_created_by_id($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('tbl_post_created');
    }
       
    
    /* End Post created Model*/
    
    
     /* Post vacant Model*/
    
    public function select_all_post_vacant_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_post_vacant');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_post_vacant_info($data)
    {
        $this->db->insert('tbl_post_vacant',$data);
    }
    
    public function select_post_vacant_by_id($post_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_post_vacant');
      $this->db->where('post_id',$post_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_post_vacant_by_id($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('tbl_post_vacant');
    }  
    
    /* End Post created Model*/
    
    /* Result Model*/
    public function select_all_result_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_result');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_result_info($data)
    {
        $this->db->insert('tbl_result',$data);
    }
     public function select_result_by_id($result_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_result');
      $this->db->where('result_id',$result_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_result_by_id($result_id)
    {
        $this->db->where('result_id',$result_id);
        $this->db->delete('tbl_result');
    }  
    /* End Result Model*/
    
    
    
    
    /* Class Routine Model*/
    
    public function select_all_routine_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_class_routine');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function save_class_routine_info($data)
    {
        $this->db->insert('tbl_class_routine',$data);
    }
    
    public function select_routine_by_id($routine_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_class_routine');
      $this->db->where('routine_id',$routine_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_routine_by_id($routine_id)
    {
        $this->db->where('routine_id',$routine_id);
        $this->db->delete('tbl_class_routine');
    }  
    
     /* End Class Routine Model*/
    
    /* Exam Routine Model*/
    
    public function select_all_exam_routine_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_routine');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_exam_routine_info($data)
    {
        $this->db->insert('tbl_exam_routine',$data);
    }
    
    public function select_exam_routine_by_id($routine_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_routine');
      $this->db->where('routine_id',$routine_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_exam_routine_by_id($routine_id)
    {
        $this->db->where('routine_id',$routine_id);
        $this->db->delete('tbl_exam_routine');
    }  
    
    
    /* End Exam Routine Model*/
    
    /* Exam seat Model*/
    public function select_all_exam_seat_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_seat');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function save_exam_seat_info($data)
    {
        $this->db->insert('tbl_exam_seat',$data);
    }
    
    public function select_exam_seat_by_id($exam_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_exam_seat');
      $this->db->where('exam_id',$exam_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_exam_seat_by_id($exam_id)
    {
        $this->db->where('exam_id',$exam_id);
        $this->db->delete('tbl_exam_seat');
    }  
    /* End Exam seat Model*/
    
    /* Syllabus Model*/
    
    public function select_all_syllabus_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_syllabus');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_syllabus_info($data)
    {
        $this->db->insert('tbl_syllabus',$data);
    }
    
    public function select_syllabus_by_id($syllabus_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_syllabus');
      $this->db->where('syllabus_id',$syllabus_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_syllabus_by_id($syllabus_id)
    {
        $this->db->where('syllabus_id',$syllabus_id);
        $this->db->delete('tbl_syllabus');
    }  
    /* End Syllabus Model*/
    
    
    /* Book list Model*/
    
    
    public function select_all_book_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_book_list');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function save_book_info($data)
    {
        $this->db->insert('tbl_book_list',$data);
    }
    
    public function select_book_by_id($book_id)
    {
      $this->db->select('*');
      $this->db->from('tbl_book_list');
      $this->db->where('book_id',$book_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
    }
    
    public function delete_book_by_id($book_id)
    {
        $this->db->where('book_id',$book_id);
        $this->db->delete('tbl_book_list');
    }  
    
    
    
     /* Book list Model*/
}

?>