<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a>&#187;<?php echo $title?></p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">

    <div class="main-content page-content">

        <div class="inner-content-wrapper">

            
            <div class="p">
                <h2 style="text-align: justify"><?php echo $history_info->history_title;?></h2>
                <hr>
                <p style="text-align: justify"><?php echo $history_info->history_description;?></p>               
                
            </div>

        </div>

    </div>
    <?php echo $right_side_bar; ?>
</div>