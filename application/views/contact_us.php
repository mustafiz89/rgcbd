<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;<?php echo $title; ?></p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">

    <!-- BEGIN .main-content -->
    <div class="main-content page-content">

        <!-- BEGIN .inner-content-wrapper -->
        <div class="inner-content-wrapper">
            
                <div class="content-block">
                    <h2>Collage Address</h2>
                    <hr style=" ">
                    <h6>Collage Name</h6>
                 <h6>Address</h6>
                 <h6>City</h6>
                 <h6>Email</h6>
                 <h6>Contact</h6>
                </div>
            
<!--            
            <div class="content-block">
                
               
                <br>
                <ul>
                    <li>School name</li>
                    <li>Address</li>
                    <li>city</li>
                    <li>Email</li>
                    <li>Phone</li>
                </ul>

                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <div id="google-map" style="width:100%;height:200px;"></div>

                <script type="text/javascript">

                    var latlng = new google.maps.LatLng(0, 0);
                    var myOptions = {
                        zoom: 14,
                        center: latlng,
                        scrollwheel: true,
                        scaleControl: false,
                        disableDefaultUI: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    var map = new google.maps.Map(document.getElementById("google-map"),
                            myOptions);

                    var geocoder_map = new google.maps.Geocoder();
                    var map_address = 'Evergreen Education School, 12, Joykali Mondir Road, Wari, Dhaka 1203, Bangladesh ';
                    geocoder_map.geocode({'address': map_address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);

                            var marker = new google.maps.Marker({
                                map: map,
                                position: map.getCenter()
                            });
                            var contentString = "<h4>StyleMag</h4><p>1 London Road, SE1</p>";

                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map, marker);
                            });
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                </script>

                 END .content-block 	
            </div>
            
            -->
            <br>
            <br>
            <form action="#" id="contactform" class="clearfix" method="post">

                <div class="field-row">
                    <label for="contact_name">Name <span>(required)</span></label>
                    <input type="text" name="contact_name" id="contact_name" class="text_input" value="" />
                </div>

                <div class="field-row">
                    <label for="email">Email <span>(required)</span></label>
                    <input type="text" name="email" id="email" class="text_input" value="" />		
                </div>

                <div class="field-row">
                    <label for="message_text">Message</label>
                    <textarea name="message" id="message_text" class="text_input" cols="60" rows="9"></textarea>
                </div>

                <input class="button1" type="submit" value="Send Email" id="submit" name="submit">

            </form>

            <!-- END .inner-content-wrapper -->
        </div>

        <!-- END .main-content -->
    </div>

    <!-- BEGIN .sidebar-right -->
    
    <?php echo $right_side_bar;?>

    <!-- END .content-wrapper -->
</div>
