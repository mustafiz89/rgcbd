<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;Photo Gallary</p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">

    <!-- BEGIN .main-content-full -->
    <div class="main-content-full page-content">

        <!-- BEGIN .inner-content-wrapper -->
        <div class="inner-content-wrapper">

            <div id='gallery-1' class='gallery gallery-columns-4'>

                <?php 
                foreach($all_photo as $v_photo)
                {
                    
                
                ?>
                <dl class='gallery-item'>	
                    <dt class='gallery-icon'>
                    <a href="<?php echo base_url(). $v_photo->image;?>" rel="prettyPhoto[gallery]"><img src="<?php echo base_url(). $v_photo->image;?>" alt="" /></a>
                    </dt>
                </dl>

                <?php 
                }
                ?>
                

                <br style='clear: both;' />

            </div>

            <!-- END .inner-content-wrapper -->
        </div>

        <!-- END .main-content-full -->
    </div>

    <!-- END .content-wrapper -->
</div>
