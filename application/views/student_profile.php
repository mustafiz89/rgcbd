<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;Student Profile</p>
        </div>

    </div>

    <!-- END .page-header -->
</div>
<style>
    .p{
        margin: 0px;
        padding: 0px;
    }
</style>
<div class="content-wrapper page-content-wrapper clearfix">

    <div class="main-content page-content">
        <div class="">
            <div class="inner-content-wrapper">
                <h3>Student Profile</h3>
                <hr>

                <div class="student-finder-full-form">

                    <h5>Search A student </h5>
                    <form action="<?php echo base_url(); ?>welcome/student_profile" method="post">
                        <table >					  
                            <h3 style="color: red; font-size: 14px;"> <?php
                                $msg = $this->session->userdata('exception');
                                if ($msg) {
                                    echo $msg;
                                    $this->session->unset_userdata('exception');
                                }
                                ?> </h3>
                            <tr>
                                <th>Class :</th>
                                <td >
                                    <select name="class_id" required>
                                        <option value="">--Select One--</option>
                                        <?php
                                        foreach ($all_class as $v_class) {
                                            ?>
                                            <option value="<?php echo $v_class->class_id; ?>"><?php echo $v_class->class_name ?></option>
                                            <?php
                                        }
                                        ?>    
                                    </select>    
                                </td>
                            </tr>
                            <tr>
                                <th>Roll :</th>
                                <td >
                                    <input type="text" name="s_roll" required value="">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" class="submi" value="Search"/></td>
                            </tr>
                        </table>
                        
                        
                    <?php
                    if(isset($s_info))
                    {
//                   ?>
                <table width="100%">
                    
                    <tr>
                        <th colspan="2">Student Information</th>                       
                    </tr>
                    
                    <tr>
                        <td>
                            <img src="<?php echo base_url().$s_info->image?>" width="200px" height="200px">
                            
                        </td>
<!--                        <td></td>
                        <td></td>
                        <td></td>-->
                        
                        <td> 
                            <p>Name : <?php echo $s_info->s_name?></p>
                            <p>Class : <?php echo $s_info->class_name?></p>
                            <p>Roll : <?php echo $s_info->s_roll?></p>
                            <p>Academic Year : <?php echo$s_info->s_year?></p>
                        
                        
                        
                        
                        </td>
                    </tr>
                    
                </table>
                <?php 
                    }                                             
                ?>
                          
                            
                       
                    </form>
                    
                  
                </div>
                



            </div>
        </div>
                
    </div>
    <?php echo $right_side_bar; ?>
</div>