<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a>Principal Message &#187;</p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">

    <div class="main-content page-content">

        <div class="inner-content-wrapper">

            <div class="image">
                <img src="<?php echo base_url().$principal_message->image; ?>" width='200' height='250'/>
            </div>
            <div class="p">
                <p style="text-align: justify"><?php echo $principal_message->message_long_list;?></p>
                <h4 style=""><?php echo $principal_message->principal_name;?><br><span>(principle)</span></h4>
                
            </div>

        </div>

    </div>
    <?php echo $right_side_bar; ?>
</div>