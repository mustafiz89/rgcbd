<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;News & Events</p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">
		
		<!-- BEGIN .main-content -->
		<div class="main-content page-content">
			
			<!-- BEGIN .inner-content-wrapper -->
			<div class="inner-content-wrapper">
				
				<ul class="event-list-full">
				
					<!-- BEGIN .event-wrapper -->
					<li class="event-wrapper event-full clearfix">
					
						
				
						<?php 
                                                foreach($all_info as $v_info)
                                                {
                                                ?>

						<div class="event-info">	
							<div class="event-meta">
								<h2><a href="<?php echo base_url();?>welcome/news_events_details/<?php echo $v_info->news_id;?>"><?php echo $v_info->news_title;?> &raquo;</a></h2>
                                                                <p>Date: <?php echo date("F j, Y", strtotime($v_info->news_date));?></p>
                                                                <p style="text-align: justify"><?php echo $v_info->news_short_description;?></p>
                                                                <p style=""><a href="<?php echo base_url();?>welcome/news_events_details/<?php echo $v_info->news_id;?>" class="readmore">Read More</a></p>
                                                                <hr>
                                                                <br>
                                                                <br>
							</div>
                                                    
						</div>

                                                
                                                <?php 
                                                }
                                                ?>
					<!-- END .event-wrapper -->
					</li>
				
					
					
					
					
					
				</ul>
			
				<div class="pagination-wrapper" style="margin:10px 0 50px 0;">
                                    
                                     <?php echo $this->pagination->create_links(); ?> 
<!--					<a class="selected" href="#">1</a>
					<a href="#">2</a>
					<a href="#">3</a>
					<a href="#">></a>-->
				</div>
			
			<!-- END .inner-content-wrapper -->
			</div>
			
		<!-- END .main-content -->
		</div>
		
		<!-- BEGIN .sidebar-right -->

<?php echo $right_side_bar;?>	
	
	<!-- END .content-wrapper -->
	</div>
	