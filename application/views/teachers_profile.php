<div class="page-header clearfix">
		
		<div class="page-header-inner clearfix">
		
		<div class="page-title">	
			<h2><?php echo $title;?></h2>
			<div class="page-title-block"></div>
		</div>
		
		<div class="breadcrumbs">
			<p><a href="<?php echo base_url();?>welocme/index.aspx">Home</a> &#187;Teacher's Profile</p>
		</div>
		
		</div>
		
	<!-- END .page-header -->
	</div>



<div class="content-wrapper page-content-wrapper clearfix">
		
		<!-- BEGIN .main-content-full -->
		<div class="main-content-full page-content">
			
			<!-- BEGIN .inner-content-wrapper -->
			<div class="inner-content-wrapper">
				
				<ul class="teacher-4">
					<?php 
                                        foreach ($all_teacher_info as $v_info)
                                        {
                                        ?>
					<li>
						<img src="<?php echo base_url().$v_info->image;?>" onerror="handleImgError(this)" alt="" width="120" height="200"/>
                                                <p></p>
                                                <h3 style=" font-size: 14px; text-align: center; font-weight: bold;"><?php echo $v_info->name;?></h3>
                                                <h4 style="font-size: 13px; font-weight: bold; text-align:center; ">(<?php echo $v_info->title;?>)</h4>
                                                <h4 style="font-size: 13px; font-weight: bold; text-align:center; "><?php echo $v_info->subject;?></h4>
                                                <h4 style="font-size: 13px; font-weight: bold; text-align:center; ">Join Date:<?php echo $v_info->join_date;?></h4>
                                                <p style="font-size: 13px; text-align:center; font-weight: bold;">Blood Group: <?php echo $v_info->blood_group;?></p>
                                                <ul class="teacher-contact">
							<li ><span class="contact-phone"><?php echo $v_info->contact;?></span></li>
							<li><span class="contact-email"><?php echo $v_info->email;?></span></li>
						</ul>
                                               
					</li>
                                        <?php 
                                        }
                                        ?>
					
				</ul>
				
				<div class="pagination-wrapper dotted-pagination" style="margin: 10px 0 10px 0 !important;">
					 <?php echo $this->pagination->create_links(); ?> 
				</div>
			
			<!-- END .inner-content-wrapper -->
			</div>
			
		<!-- END .main-content-full -->
		</div>
	
	<!-- END .content-wrapper -->
	</div>
	