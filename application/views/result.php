<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;<?php echo $title;?></p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">

    <div class="main-content page-content">

        <div class="inner-content-wrapper">
            <h3><?php echo $title;?></h3>
                <hr>
                
                <table border="" style="width:100%">

            <?php 
            $i=0;
            foreach($result_info as $v_info)
            {
              $i+=1;           
            ?>
            
            
                
                    <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $v_info->result_description?></td>
                        <td><?php echo date("F j, Y", strtotime($v_info->result_date));?></td>
                        <td><a style="color:green" href="<?php echo base_url().$v_info->result_file;?>">(Download)</a></td>
                    </tr>
                
                         
            
            <?php
            }
            ?>
                    </table>
        </div>

    </div>
    <?php echo $right_side_bar; ?>
</div>