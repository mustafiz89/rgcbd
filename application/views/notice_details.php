<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;Notice Details</p>
        </div>

    </div>

    <!-- END .page-header -->
</div>

<div class="content-wrapper page-content-wrapper clearfix">
		
		<!-- BEGIN .main-content -->
		<div class="main-content page-content">
			
			<!-- BEGIN .inner-content-wrapper -->
			<div class="inner-content-wrapper">
            	
				<ul class="event-list-full">
				
					<!-- BEGIN .event-wrapper -->
					<li class="event-wrapper event-full clearfix">
					
<!--						<div class="event-date">
							<div class="event-m">Month</div>
							<div class="event-d">Date</div>	
						</div>-->
                                                		<div class="event-info">	
							<div class="event-meta">
								<h2><?php echo $all_info->notice_title;?></h2>
                                                                <p>Date: <?php echo date("F j, Y ", strtotime($all_info->notice_date));?></p>
                                                                <hr>
                                                                <p style="text-align: justify"><?php echo $all_info->notice_long_description;?></p>
                                                               
							</div>
                                                    
						</div>
                                                
                                               

					<!-- END .event-wrapper -->
					</li>
						
					<!-- BEGIN .event-wrapper -->
				</ul>
			
				
			
			<!-- END .inner-content-wrapper -->
			</div>
			
		<!-- END .main-content -->
		</div>
		
		<!-- BEGIN .sidebar-right -->

                <?php echo $right_side_bar;?>
	
	<!-- END .content-wrapper -->
	</div>
	
	