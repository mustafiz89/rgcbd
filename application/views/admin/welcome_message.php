<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Welcome Message</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>welcome Short Message</th>
                        <th>Welcome Long Message</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    foreach ($welcome_info as $v_welcome) {
                                             
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $v_welcome->welcome_id ;?></td>
                            <td class="center" ><?php echo $v_welcome->welcome_short_message;?></td>
                            <td class="center" ><?php echo $v_welcome->welcome_long_message;?></td>
                             <?php
                        }
                    ?>
                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url();?>administrator/welcome_message_edit/<?php echo $v_welcome->welcome_id;?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>                                
                            </td>
                        </tr>
                   

                </tbody>
            </table>
        </div>
    </div>
</div>
