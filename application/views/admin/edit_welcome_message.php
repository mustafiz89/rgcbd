<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Welcome message</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            
            <form class="form-horizontal" action="<?php echo base_url();?>administrator/update_welcome_message" method="post" >
                <fieldset>
                    <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="welcome_id" value="<?php echo $edit_welcome_info->welcome_id; ?>">
                    <div class="control-group">
                        
                        <label class="control-label" for="textarea2">Welcome Short Message(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" style="width:400px; height:50px;" maxlength="50" name="welcome_short_message" id="textarea2" required rows="3"><?php echo $edit_welcome_info->welcome_short_message; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Welcome Long Message(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" style="width:400px; height:150px;"  name="welcome_long_message" id="textarea2" required rows="3"><?php echo $edit_welcome_info->welcome_long_message;?></textarea>
                        </div>
                    </div>
                    
                                         
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->