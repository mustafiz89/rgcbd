


<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Student Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>
            
            <form class="form-horizontal" action="<?php echo base_url(); ?>student/save_student" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Student Name (<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" data-source='' name="s_name" value="">
                            
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Academic Year(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' name="s_year" value="">
                            
                        </div>
                    </div>
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Class(<span class="required">*</span>)</label>
                        <div class="controls">
                            <select id="selectError3" name="class_id" required="required" >
                                <option value="">Select Class</option>
                                <?php
                                foreach($all_class as $v_class)
                                {        
                                ?>
                                <option value="<?php echo $v_class->class_id;?>"><?php echo $v_class->class_name ?></option>
                                <?php
                                }
                                ?>                             
                               
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Student Roll (<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' name="s_roll" value="">
                            
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Father's Name (<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' name="f_name" value="">
                            
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Mother's Name (<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' name="m_name" value="">
                            
                        </div>
                    </div>
                    
                    
                    
                    
                    
                     <div class="control-group">
                        <label class="control-label" for="selectError3">Religion(<span class="required">*</span>)</label>
                        <div class="controls">
                            <select id="selectError3" name="s_religion" required="">
                                <option value="">Select</option>
                                <option value="islam">Islam</option>
                                <option value="hindu">Hindu</option>
                                <option value="bouddo">Bouddo</option>
                                <option value="chirstian">Christian</option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                    </div>
                    
                     <div class="control-group">
                        <label class="control-label" for="selectError3">Sex(<span class="required">*</span>)</label>
                        <div class="controls">
                            <select id="selectError3" name="s_sex" required="required">
                                <option value="">Select</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>                                
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Birth Date(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="input-xlarge datepicker" id="date01" name="s_birth_date" value="" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Contact(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' name="s_contact" value="">
                            
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label" for="textarea2">Present Address(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="present_address" id="textarea2" required rows="3" style="width:385px; height:80px;" maxlength="250"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Parmanent Address(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="parmanent_address" id="textarea2" required rows="3" style="width:385px; height:80px;" maxlength="250"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Image(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input class="input-file uniform_on" id="fileInput" type="file" required="file" name="image" accept="image/*">
                        </div>
                    </div> 
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>
            
        </div>
    </div><!--/span-->

</div><!--/row-->
