<!DOCTYPE html>
<html lang="en">
    <head>
        <!--
                Copyright 2014 Mustafizur rahman		
        -->
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mustafizur rahman">

        <script type="text/javascript">

            function check_delete()
            {
                var chk = confirm('Are you sure to delete this!');
                if (chk)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
           
        </script>
        
        
       
        <script type="text/javascript" src="<?php echo base_url(); ?>script/jsval.js"></script>
        <!-- The styles -->
        <link id="" href="<?php echo base_url(); ?>css/bootstrap-cerulean.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
            .required{
                color:red;
                font-size: 16px;
            }
            
            
        </style>
        <link href="<?php echo base_url(); ?>css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='<?php echo base_url(); ?>css/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='<?php echo base_url(); ?>css/chosen.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/uniform.default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/jquery.cleditor.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/opa-icons.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>css/uploadify.css' rel='stylesheet'>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.ico">


    </head>

    <body>
        <!-- topbar starts -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#"><span><p style="color: white;">রামগঞ্জ সরকারি কলেজ</p></span></a>


                    <!-- user dropdown starts -->
                    <div class="btn-group pull-right" >
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user"></i><span class="hidden-phone"><?php echo $this->session->userdata('admin_name') ?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url() ?>administrator<?php //echo $admin_id=$this->session->userdata('admin_id'); ?>">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url() ?>administrator/logout">Logout</a></li>
                        </ul>
                    </div>
                    <!-- user dropdown ends -->

                </div>
            </div>
        </div>
        <!-- topbar ends -->
        <div class="container-fluid">
            <div class="row-fluid">

                <!-- left menu starts -->
                <div class="span2 main-menu-span">
                    <div class="well nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li class="nav-header hidden-tablet">Main</li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/index.aspx"><i class="icon-edit"></i><span class="hidden-tablet"> Dashboard</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_slider"><i class="icon-edit"></i><span class="hidden-tablet">Slider Image</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/welcome_message"><i class="icon-edit"></i><span class="hidden-tablet">Welcome Message</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/collage_history"><i class="icon-edit"></i><span class="hidden-tablet">College History</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/principal_message"><i class="icon-edit"></i><span class="hidden-tablet">Principle Message</span></a></li>
                            <!--<li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_governing_body"><i class="icon-edit"></i><span class="hidden-tablet">Governing Body</span></a></li>-->
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_citizen_charter"><i class="icon-edit"></i><span class="hidden-tablet">Citizen Charter</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_teacher"><i class="icon-edit"></i><span class="hidden-tablet">Teacher's List</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>student/manage_class"><i class="icon-edit"></i><span class="hidden-tablet">Student Class</span></a></li>

                            <li><a class="ajax-link" href="<?php echo base_url(); ?>student/add_student"><i class="icon-edit"></i><span class="hidden-tablet">Add Student</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>student/manage_student"><i class="icon-edit"></i><span class="hidden-tablet">Manage Student</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_3rd_staff"><i class="icon-edit"></i><span class="hidden-tablet">Staff's List(3rd class)</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_4th_staff"><i class="icon-edit"></i><span class="hidden-tablet">Staff's List(4th class)</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_notice"><i class="icon-edit"></i><span class="hidden-tablet">Manage Notice</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_news"><i class="icon-edit"></i><span class="hidden-tablet">Manage News</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/alternate_teacher"><i class="icon-edit"></i><span class="hidden-tablet">Alternate Teacher</span></a></li>

                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/academic_calender"><i class="icon-edit"></i><span class="hidden-tablet">Academic Calender</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/admission_info"><i class="icon-edit"></i><span class="hidden-tablet">Admission Info</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/admission_form"><i class="icon-edit"></i><span class="hidden-tablet">Admission Form</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/post_created"><i class="icon-edit"></i><span class="hidden-tablet">Post (Created)</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/post_vacant"><i class="icon-edit"></i><span class="hidden-tablet">Post (Vacant)</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/result.aspx"><i class="icon-edit"></i><span class="hidden-tablet"> Result</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/class_routine.aspx"><i class="icon-edit"></i><span class="hidden-tablet"> Class Routine</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/exam_routine.aspx"><i class="icon-edit"></i><span class="hidden-tablet">Exam Routine</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/exam_seat.aspx"><i class="icon-edit"></i><span class="hidden-tablet"> Exam Seat</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/syllabus"><i class="icon-edit"></i><span class="hidden-tablet">Syllabus</span></a></li>
                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator2/book_list"><i class="icon-edit"></i><span class="hidden-tablet">Book List</span></a></li>

                            <li><a class="ajax-link" href="<?php echo base_url(); ?>administrator/manage_photo_gallary"><i class="icon-edit"></i><span class="hidden-tablet">Photo Gallery</span></a></li>
                           
<!--					<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
                    </div><!--/.well -->
                </div><!--/span-->
                <!-- left menu ends -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="span10">
                    <!-- content starts -->
                    <?php
                    echo $admin_mid_content;
                    ?>




                    <!-- content ends -->
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->

            <hr>

            <div class="modal hide fade" id="myModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary">Save changes</a>
                </div>
            </div>

            <footer>
                <p class="pull-left">&copy; <a href="" target="_blank">Mustafizur Rahman</a> 2015</p>
                <p class="pull-right">Powered by: <a href="">Dynamic Software Ltd</a></p>
            </footer>

        </div><!--/.fluid-container-->

        <!-- external javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
        <!-- jQuery UI -->
        <script src="<?php echo base_url(); ?>js/jquery-ui-1.8.21.custom.min.js"></script>
        <!-- transition / effect library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-transition.js"></script>
        <!-- alert enhancer library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-alert.js"></script>
        <!-- modal / dialog library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-modal.js"></script>
        <!-- custom dropdown library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-dropdown.js"></script>
        <!-- scrolspy library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-scrollspy.js"></script>
        <!-- library for creating tabs -->
        <script src="<?php echo base_url(); ?>js/bootstrap-tab.js"></script>
        <!-- library for advanced tooltip -->
        <script src="<?php echo base_url(); ?>js/bootstrap-tooltip.js"></script>
        <!-- popover effect library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-popover.js"></script>
        <!-- button enhancer library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-button.js"></script>
        <!-- accordion library (optional, not used in demo) -->
        <script src="<?php echo base_url(); ?>js/bootstrap-collapse.js"></script>
        <!-- carousel slideshow library (optional, not used in demo) -->
        <script src="<?php echo base_url(); ?>js/bootstrap-carousel.js"></script>
        <!-- autocomplete library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js"></script>
        <!-- tour library -->
        <script src="<?php echo base_url(); ?>js/bootstrap-tour.js"></script>
        <!-- library for cookie management -->
        <script src="<?php echo base_url(); ?>js/jquery.cookie.js"></script>
        <!-- calander plugin -->
        <script src='<?php echo base_url(); ?>js/fullcalendar.min.js'></script>
        <!-- data table plugin -->
        <script src='<?php echo base_url(); ?>js/jquery.dataTables.min.js'></script>

        <!-- chart libraries start -->
        <script src="<?php echo base_url(); ?>js/excanvas.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.flot.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.flot.pie.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.flot.stack.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.flot.resize.min.js"></script>
        <!-- chart libraries end -->

        <!-- select or dropdown enhancer -->
        <script src="<?php echo base_url(); ?>js/jquery.chosen.min.js"></script>
        <!-- checkbox, radio, and file input styler -->
        <script src="<?php echo base_url(); ?>js/jquery.uniform.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo base_url(); ?>js/jquery.colorbox.min.js"></script>
        <!-- rich text editor library -->
        <script src="<?php echo base_url(); ?>js/jquery.cleditor.min.js"></script>
        <!-- notification plugin -->
        <script src="<?php echo base_url(); ?>js/jquery.noty.js"></script>
        <!-- file manager library -->
        <script src="<?php echo base_url(); ?>js/jquery.elfinder.min.js"></script>
        <!-- star rating plugin -->
        <script src="<?php echo base_url(); ?>js/jquery.raty.min.js"></script>
        <!-- for iOS style toggle switch -->
        <script src="<?php echo base_url(); ?>js/jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo base_url(); ?>js/jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo base_url(); ?>js/jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo base_url(); ?>js/jquery.history.js"></script>
        <!-- application script for Charisma demo -->
        <script src="<?php echo base_url(); ?>js/charisma.js"></script>


    </body>
</html>
