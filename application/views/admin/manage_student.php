 
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Search Student</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>
            <form class="form-horizontal" action="<?php echo base_url(); ?>student/manage_student" method="post"   onsubmit="return validateForm();"  >
                <div class="control-group">
                        <label class="control-label" for="selectError3">Select Class</label>
                        <div class="controls">
                            <select  name="class_id" id="select">
                                <option value="">Select Class</option>
                                <?php
                                foreach($all_class as $v_class)
                                {        
                                ?>
                                <option value="<?php echo $v_class->class_id;?>"><?php echo $v_class->class_name ?></option>
                                <?php
                                }
                                ?>                             
                               
                            </select>
                            <button type="submit"  class="btn btn-primary" name="btn">View Student</button>
                        </div>
                         
                    </div>
                
                
                       
                        
                    
            </form>
            
            

        </div>
    </div><!--/span-->
    
    
    
    

</div><!--/row-->



<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('d_message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('d_message');
                }
                ?>

            </div>
            
            <table class="table table-striped table-bordered">
                
                <thead>
               
                    <tr>
                        <th style="text-align:center;">Roll</th>
                        <th style="text-align:center;">Student Name</th>
                        <th style="text-align:center;">Class</th>
                        <th style="text-align:center;">Year</th>
                        <th style="text-align:center;">Actions</th>
                    </tr>
                   
                </thead> 
               
                <tbody>
                    
                <tr>
                    <?php 
//                    if(isset($all_student))
//                    {
                   
                    foreach($all_student as $v_class)
                    {
                    ?>
                    
                            <td style="text-align: center;"><?php echo $v_class->s_roll ?></td>
                            <td style="text-align: center;" >
                                <?php echo $v_class->s_name;?>
                            </td>
                            
                            <td style="text-align: center;">
                                <?php echo $v_class->class_name;?>
                            </td>

                            <td style="text-align: center;">
                                <?php echo $v_class->s_year;?>
                            </td>
                            <td class="center">
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>student/delete_student/<?php echo $v_class->s_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                                <a class="btn btn-primary" href="<?php echo base_url(); ?>student/edit_student/<?php echo $v_class->s_id; ?>">
                                    <i class="icon-edit icon-white"></i> 
                                   Edit
                                </a>
                                    
<!--                                <script TYPE="text/javascript">
				function popupform(myform, windowname) {
				if (! window.focus)return true;
				window.open('', windowname, 'height=600,width=800,scrollbars=yes');
				myform.target=windowname;
				return true;
				}
                                 </script>   -->
                                
<!--                                <script>
                                function popupform(){
                                w=window.open();
                                
                                w.print();
                                w.close();
                                }
                                </script>-->

                                <SCRIPT TYPE="text/javascript">

                                        function popup(mylink, windowname)
                                        {
                                        if (! window.focus)return true;
                                        var href;
                                        if (typeof(mylink) == 'string')
                                           href=mylink;
                                        else
                                           href=mylink.href;
                                        window.open(href, windowname, 'width=900,height=500,scrollbars=yes');
                                        return false;
                                        }

                                    </SCRIPT>
                                    
                                    <a class="btn btn-success"  href="<?php echo base_url(); ?>student/view_student/<?php echo $v_class->s_id; ?>" >
                                    <i class="icon-user icon-white"></i> 
                                   View
                                </a>
                            </td>
                    </tr>
                    <?php 
                    }
//                    }
                    
//                    else{
//                    ?> 
                    
                    
                    
                    <?php 
//                    }
//                    ?>
                </tbody>
                
            </table>
           
            
            

        </div>
    </div>
</div>

