<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Chairman Message</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_principal_message" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Principle Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="principal_name" value="<?php echo $message_info->principal_name;?>">
                            <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="message_id" value="<?php echo $message_info->message_id;?>">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Short Message(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="message_short_list" id="textarea2" required rows="3" style="width:600px; height:40px;" maxlength="250"><?php echo $message_info->message_short_list;?></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Long Message(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="message_long_list" id="textarea2" required rows="3" style="width:600px; height:400px;"><?php echo $message_info->message_long_list;?></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="fileInput">Previous Image</label>
                        <div class="controls">
                            <img src="<?php echo base_url().$message_info->image;?>" width="200" height="200"> 
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="fileInput" accept="image/*" > Image</label>
                        <div class="controls">
                            <input type="file" name="image">
                        </div>
                    </div>          
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
