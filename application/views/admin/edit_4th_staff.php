<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo $title?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_4th_staff" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Name</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="name" required value="<?php echo $edit_info->name;?>">
                            <input type="hidden" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="id" required value="<?php echo $edit_info->id?>">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Designation</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="title" required value="<?php echo $edit_info->title;?>">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Contact</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" name="contact" required value="<?php echo $edit_info->contact;?>">

                        </div>
                    </div>
                    
                     <div class="control-group">
                        <label class="control-label" for="fileInput">Previous Image</label>
                        <div class="controls">
                            <img src="<?php echo base_url().$edit_info->image;?>" width="200" height="200"> 
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Image</label>
                        <div class="controls">
                            <input class="input-file uniform_on" id="fileInput" name="image" type="file"><label><span style="color: green;">(N.B:File size should not be more than 3 MB and 2000*1200 pixel)</span></label>
                        </div>
                    </div>      

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

