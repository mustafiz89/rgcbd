<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo $title?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            
            <form class="form-horizontal" action="<?php echo base_url();?>administrator/update_citizen_charter" method="post" >
                <fieldset>
                    <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="charter_id" value="<?php echo $charter_info->charter_id; ?>">
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Description(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" style="width:600px; height:300px;"  name="Description" id="textarea2" required rows="3"><?php echo $charter_info->description;?></textarea>
                        </div>
                    </div>
                    
                                         
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->