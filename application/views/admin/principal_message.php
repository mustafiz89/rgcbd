<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Chairman Message</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Principle Name</th>
                        <th>Principle Message</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    foreach ($message_info as $v_message) {
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $v_message->message_id ;?></td>
                            <td class="center" ><?php echo $v_message->principal_name;?></td>
                            <td class="center" ><?php echo $v_message->message_short_list;?></td>
                            <td class="center">
                                <img src="<?php echo base_url().$v_message->image;?>" width="100" height="100" >
                            </td>
                            
                             <?php
                        }
                    ?>
                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url();?>administrator/principal_message_edit/<?php echo $v_message->message_id;?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>                                
                            </td>
                        </tr>
                   

                </tbody>
            </table>
        </div>
    </div>
</div>
