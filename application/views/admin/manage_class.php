
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo $title;?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>student/save_class" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    
                    
                   <div class="control-group">
                        <label class="control-label" for="typeahead">Class Name(<span class="required">*</span>)</label>
                        <div class="controls">
                              <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' required name="class_name" value="">

                        </div>
                    </div>
                    
                                      
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->
    
    
    
    

</div><!--/row-->



<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 20px">
                <?php
                $msg = $this->session->userdata('d_message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('d_message');
                }
                ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style=" text-align: center;">No</th>                       
                        <th style=" text-align: center;">Class Name</th>
                        <th style=" text-align: center;">Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    $i = 0;
                    foreach ($all_info as $v_info) {
                        $i+=1;
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i; ?></td>
                                                        
                            <td class="center" style="text-align: center;">
                                <?php echo $v_info->class_name;?>
                            </td>

                           
                            <td class="center">
                                <a class="btn btn-success" href="<?php echo base_url(); ?>student/edit_class/<?php echo $v_info->class_id; ?>" >
                                    <i class="icon-edit icon-white"></i> 
                                    Edit
                                </a>
                                <a class="btn btn-warning" href="<?php echo base_url(); ?>student/delete_class/<?php echo $v_info->class_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                            
                    </tr>
                    <?php
                        }
                        ?>

                </tbody>
            </table>

            

        </div>
    </div>
</div>

