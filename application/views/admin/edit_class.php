<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo $title;?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>student/update_class" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    
                    
                   <div class="control-group">
                        <label class="control-label" for="typeahead">Class Name(<span class="required">*</span>)</label>
                        <div class="controls">
                              <input type="text" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' required name="class_name" value="<?php echo $class_info->class_name;?>">
                              <input type="hidden" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='' required name="class_id" value="<?php echo $class_info->class_id;?>">

                        </div>
                    </div>
                    
                                      
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->
    
    
    
    

</div><!--/row-->
