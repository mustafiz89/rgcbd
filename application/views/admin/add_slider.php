<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Slider</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 16px;">
            <?php 
             $msg=$this->session->userdata('message');
             if($msg)
             {
               echo $msg;
               $this->session->unset_userdata('message');
             }
            ?>
            
            </div>
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/save_slider" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        
                        <label class="control-label" for="fileInput">Slider Image</label>
                        <div class="controls">
                            <input type="file" accept="image/*" name="slider_image"><label><span style="color: green;">(N.B:File size should not be more than 3 MB and 2000*1000 pixel)</span></label>
                        </div>
                    </div>          
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->