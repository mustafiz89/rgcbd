
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo $title?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 16px;">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>

            </div>

            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator2/save_post_created" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    
                    
                   <div class="control-group">
                        <label class="control-label" for="textarea2">Description(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="post_description" id="textarea2" required rows="3" style="width:600px; height:40px;"></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        
                        <label class="control-label" for="fileInput">Upload File(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="file" name="post_file" required="file" accept=""><label><span style="color: green;"></span></label>
                        </div>
                    </div>
                    
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->
    
    
    
    

</div><!--/row-->



<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 20px">
                <?php
                $msg = $this->session->userdata('d_message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('d_message');
                }
                ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Description</th>
                        <th>File Name</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    $i = 0;
                    foreach ($all_info as $v_info) {
                        $i+=1;
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i; ?></td>
                            <td class="center" >
                                <?php echo $v_info->post_description;?>
                            </td>
                            
                            <td class="center">
                                <?php echo $v_info->post_file;?>
                            </td>

                            <td class="center">
                                <?php echo date("F j, Y", strtotime($v_info->post_date));?>
                            </td>
                            <td class="center">
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator2/delete_post_created/<?php echo $v_info->post_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                            
                    </tr>
                    <?php
                        }
                        ?>

                </tbody>
            </table>

            

        </div>
    </div>
</div>

