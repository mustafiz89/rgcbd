<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Staff's(3rd Class) Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">

            <fieldset>
                <legend>Add Information</legend>
                <a class="btn btn-success" href="<?php echo base_url(); ?>administrator/add_3rd_staff">
                    <i class="icon-edit icon-white"></i>  
                    Add Staff's                                       
                </a>  
            </fieldset>


        </div>
    </div><!--/span-->

</div><!--/row-->
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Information</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <div style="color:green; font-size: 20px">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Information</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    $i = 0;
                    foreach ($all_staff_info as $v_info) {
                        $i+=1;
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i; ?></td>
                            <td class="center" >
                                <label>Name: <?php echo $v_info->name; ?></label>
                                <label>Designation : <?php echo $v_info->title; ?></label>
                                <label>Contact: <?php echo $v_info->contact; ?></label>
                              




                            </td>

                            <td class="center">
                                <img src="<?php echo base_url() . $v_info->image; ?>" width="100" height="100" >
                            </td>


                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url(); ?>administrator/edit_3rd_staff/<?php echo $v_info->id; ?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>  
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_3rd_staff/<?php echo $v_info->id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                            
                    </tr>
                    <?php
                        }
                        ?>

                </tbody>
            </table>

            <div class="pagination pagination-centered">
                <div>
                    <ul>
                        
                            
                            <?php echo $this->pagination->create_links(); ?> 
                        
                         
                    
                   

                </ul>
                </div>
                
            </div>

        </div>
    </div>
</div>

