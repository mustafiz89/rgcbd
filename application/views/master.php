<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if lt IE 7]> <html dir="ltr" lang="en-US" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html dir="ltr" lang="en-US" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html dir="ltr" lang="en-US" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html dir="ltr" lang="en-US"> <!--<![endif]-->

    <!-- BEGIN head -->
    <head>

        <!--Meta Tags-->
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

            <!--Title-->
            <title>রামগঞ্জ সরকারি কলেজ</title>

            <!--Stylesheets-->
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css"  media="all" />
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/colour.css" type="text/css"  media="all" />
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/flexslider.css" type="text/css"  media="all" />
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/superfish.css" type="text/css"  media="all" />
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/responsive.css" type="text/css"  media="all" />
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,700,900' rel='stylesheet' type='text/css'>

                    <!--Favicon-->
                    <link rel="shortcut icon" href="<?php echo base_url();?>favicon.html" type="image/x-icon" />

                    <!--JavaScript-->
            <!--	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
                    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'></script>-->
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.7.min.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/superfish.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexslider-min.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/tinynav.min.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.uniform.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>js/scripts.js"></script>

                    <!--[if (gte IE 6)&(lte IE 8)]>
                            <script type="text/javascript" src="js/selectivizr-min.js"></script>
                    <![endif]-->

                    <!-- END head -->

                    <script type="text/javascript">
                        function handleImgError(srcElement)
                        {
                            srcElement.src = "images/no.jpg";
                            srcElement.onerror = "";
                            return true;
                        }
                    </script>
                    </head>
                    <body id="top" class="loading">
                        <div id="header-wrapper">

                            <!-- BEGIN #header-border -->
                            <div id="header-border">

                                <!-- BEGIN #header-top -->
                                <div id="header-top" class="clearfix">

                                    <!--<ul class="top-left-nav clearfix">
                                        <li><a href="blog.php">Blog</a><span>/</span></li>
                                        <li><a href="events.php">Events</a><span>/</span></li>
                                        <li><a href="typography.php">Jobs</a><span>/</span></li>
                                        <li><a href="courses.php">Courses</a><span>/</span></li>
                                        <li><a href="typography.php">Campus</a><span>/</span></li>
                                    </ul>-->

                                    <ul class="top-right-nav clearfix">
                                        <li class="phone-icon">01706-889358</li>
                                        <li class="email-icon">ramgonj_college@yahoo.com</li>
                                    </ul>

                                    <!-- END #header-top -->
                                </div>

                                <!-- BEGIN #header-content-wrapper -->
                                <div id="header-content-wrapper" class="clearfix">

                                    <div id="logo">
                                        <a href="<?php echo base_url(); ?>welcome/index.aspx"><img src="<?php echo base_url(); ?>images/logo/logo.jpg" height="100px" width="100px" /></a>
                                    
                                    </div>
                                    
                                    <div id="name-text">
                                        <p style="font-size: 50px; color:#00693E; font-weight: bold;" >রামগঞ্জ সরকারি কলেজ</p>

                                    </div>
                                    
                                    <div>
                                    <ul class="social-icons clearfix">	
                                        <li><a target="_blank" href="http://twitter.com"><span class="twitter-icon"></span></a></li>
                                        <li><a target="_blank" href="http://facebook.com"><span class="facebook-icon"></span></a></li>
                                        <li><a target="_blank" href="http://gmail.com"><span class="gplus-icon"></span></a></li>
                                        <li><a target="_blank" href="http://youtube.com"><span class="youtube-icon"></span></a></li>
                                        <li><a target="_blank" href="http://skype.com"><span class="skype-icon"></span></a></li>
                                    </ul>
                                    </div>
                                    <!-- END #header-content-wrapper -->
                                </div>

                                <!-- BEGIN #main-menu-wrapper -->
                                <div id="main-menu-wrapper" class="clearfix">

                                    <!-- BEGIN #main-menu -->
                                    <ul id="main-menu">

                                        <li class="current_page_item"><a href="<?php echo base_url(); ?>welcome/index.aspx">Home</a></li>
                                        <li><a href="#">About Us</a>
                                            <ul>
                                                <li><a href="<?php echo base_url(); ?>welcome/collage_history.aspx"><span>College History</span></a></li>
                                                <li><a href="<?php echo base_url(); ?>welcome/principal_message.aspx"><span>Principal Message</span></a></li>
                                                <li><a href="<?php echo base_url(); ?>welcome/citizen_charter.aspx">Citizen Charter</a></li>                    
                                                <li><a href="<?php echo base_url(); ?>welcome/teachers_profile.aspx">Teachers Profile</a></li>
                                                <li><a href="#">Our Staff's</a>
                                                    <ul>
                                                        <li><a href="<?php echo base_url(); ?>welcome/our_3rd_staff.aspx">3rd Class</a></li>
                                                        <li><a href="<?php echo base_url(); ?>welcome/our_4th_staff.aspx">4th Class</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="<?php echo base_url()?>welcome/photo_gallary.aspx">Photo Gallery</a></li>   
                                            </ul>     
                                        </li>
                                                
                                          
                                                
                                               
                                           
                                        </li>
                                        <!--<li><a href="courses.php">Courses</a>
                                            <ul>
                                                <li><a href="courses-single.php">Examine Batch</a>
                                                    <ul>
                                                        <li><a href="courses-single.php">"O" Level</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="courses-single.php">Beginners Batch</a>
                                                    <ul>
                                                        <li><a href="courses-single.php">"A" Level</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="courses-single.php">Junior Section</a>
                                                    <ul>
                                                        <li><a href="courses-single.php">Class One</a></li>
                                                        <li><a href="courses-single.php">Class Two</a></li>
                                                        <li><a href="courses-single.php">Class Three</a></li>
                                                        <li><a href="courses-single.php">Class Four</a></li>
                                                        <li><a href="courses-single.php">Class Five</a></li>
                                                        <li><a href="courses-single.php">Class Six</a></li>
                                                        <li><a href="courses-single.php">Class Seven</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>-->
                                        <li><a href="<?php echo base_url();?>welcome/student_profile">Student Profile</a></li>
                                        <!--<li><a href="#">Features</a>
                                            <ul>
                                        <!--<li><a href="portfolio.php">Portfolio</a></li>
                                        <li><a href="portfolio-single.php">Portfolio Single</a></li>-->
                                        <!--</ul>-->
                                        <!--</li>-->
                                        <li><a href="#">Result</a>
                                            <ul>
                                                <li><a href="<?php echo base_url();?>welcome/result"><span>Result</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/class_routine"><span>Class Routine</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/exam_routine"><span>Exam Routine</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/syllabus"><span>Syllabus</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/book_list"><span>Book List</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/exam_seat"><span>Exam Seat</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Other Info</a>
                                            <ul>                    
                                                <li><a href="<?php echo base_url();?>welcome/notice.aspx"><span>Notice Board</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/news_events.aspx"><span>News & Event</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/alternate_teacher">Alternate Teacher Notice</a></li>
                                                <li><a href=""><span>Download</span></a>
                                                    <ul>
                                                        <li><a href="<?php echo base_url();?>welcome/academic_calender"><span>Academic Calender</span></a></li>
                                                         
                                                        
                                                    </ul>
                                                
                                                </li>
                                                <li><a href="#"><span>Posts/Carrier</span></a>
                                                    <ul>
                                                        <li><a href=""><span>Created</span></a></li>
                                                        <li><a href=""><span>Vacant</span></a></li>
                                                        
                                                    </ul>
                                                
                                                </li>
                                                <li><a href="<?php echo base_url();?>welcome/objection_corner.aspx"><span>Objection Corner</span></a>
                                                <!--<li><a href="js-elements.php">Feed Back</a></li>-->
                                            </ul>
                                        </li>
                                        <li><a href="#">Admission</a>
                                            <ul>
                                                <li><a href="<?php echo base_url();?>welcome/admission_info"><span>Admission Info</span></a></li>
                                                <li><a href="<?php echo base_url();?>welcome/admission_form"><span>Admission Form</span></a></li>
                                            </ul>
                                                                                      
                                        </li>
                                        <li><a href="<?php echo base_url();?>welcome/contact_us.aspx">Contact Us</a></li>
                                       

                                        <!-- END #main-menu -->
                                    </ul>

                                    <!--<div class="menu-search-button"></div>-->
                                    <!--<form method="get" action="#" class="menu-search-form">
                                        <input class="menu-search-field" type="text" onBlur="if(this.value=='')this.value='To search, type and hit enter';" onFocus="if(this.value=='To search, type and hit enter')this.value='';" value="To search, type and hit enter" name="s" />
                                    </form>-->

                                    <!-- END #main-menu-wrapper -->
                                </div>

                                <!-- END #header-border -->
                            </div>

                            <!-- END #header-wrapper -->
                        </div>



                        <!--MID CONTENT-->

                        <?php echo $mid_content; ?>














                        <div id="footer-wrapper">

                            <!-- BEGIN #footer -->
                            <div id="footer">
                                
<!--
-->                                <ul class="columns-4 clearfix"><!--

-->                                    <li class="col">
                                        <div class="widget-title clearfix">

                                            <h4><a href="">Contact Us</a></h4>
                                            <div class="widget-title-block"></div>
                                        </div>
                                        <p></p>
                                    </li>
</ul>
<!--

                                    <li class="col">
                                        <div class="widget-title clearfix">
                                            <h4>Tag Cloud</h4>
                                            <div class="widget-title-block"></div>
                                        </div>

                                        <ul class="wp-tag-cloud">
                                            <li><a href="#">Education</a></li>
                                            <li><a href="#">University</a></li>
                                            <li><a href="#">College</a></li>
                                            <li><a href="#">Learning</a></li>
                                            <li><a href="#">Art</a></li>
                                            <li><a href="#">Design</a></li>
                                            <li><a href="#">Music</a></li>
                                            <li><a href="#">Environment</a></li>
                                        </ul>

                                    </li>

                                    <li class="col">
                                        <div class="widget-title clearfix">
                                            <h4>Twitter Feed</h4>
                                            <div class="widget-title-block"></div>
                                        </div>

                                        <ul id="twitter_update_list">
                                            <li>Twitter feed loading</li>
                                        </ul>

                                        <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
                                        <script type="text/javascript" src="http://api.twitter.com/1/statuses/user_timeline/quitenicestuff.json?callback=twitterCallback2&amp;count=2"></script>

                                    </li>

                                    <li class="col">
                                        <div class="widget-title clearfix">
                                            <h4>Recent Activities</h4>
                                            <div class="widget-title-block"></div>
                                        </div>

                                        <ul>
                                            <li><a href="blog-single.php">Proin imperdiet adipiscing elit</a></li>
                                            <li><a href="blog-single.php">Vestibulum blandit</a></li>
                                            <li><a href="blog-single.php">Nulla semper arcu at tincidunt</a></li>
                                            <li><a href="blog-single.php">Morbi quam arcu sagittis</a></li>
                                        </ul>

                                    </li>

                                </ul>-->

                                <div id="footer-bottom" class="clearfix">

                                    <p class="fl">&copy; Copyright - <a href="www.evergreeneducationbd.ocm"></a> by <a href="www.dynamicsoftwareltd.com">Dynamic Software Ltd</a></p>
                                    <p class="go-up fr"><a href="#top" class="scrollup">Top</a></p>
                                </div>

                                <!-- END #footer -->
                            </div>

                            <!-- END #footer-wrapper -->
                        </div>

                    </body>
                    </html>
