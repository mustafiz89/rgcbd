<div class="slider clearfix">

    <ul class="slides slide-loader">

        <!-- <li style="background:url(images/slide_01.jpg) no-repeat bottom center">
            
            <div class="flex-caption-wrapper">
                <div class="flex-caption">
                    <p>Change is the end result</p>
                    <div class="clearboth"></div>
                    <p>of all true learning.</p>
                </div>
            </div>
            
        </li>-->

        <?php 
        foreach($all_slider as $v_slider)
        {
        ?>
        <li style="background: url(<?php echo base_url().$v_slider->slider_image?>)no-repeat bottom center"></li>
        
<!--        <li style="background: url(<?php echo base_url()?>images/slide_02.jpg) no-repeat bottom center"></li>
        <li style="background: url(<?php echo base_url()?>images/slide_03.jpg) no-repeat bottom center"></li>-->

        <?php 
        }
        ?>


        <!-- <li style="background:url(images/slide_03.jpg) no-repeat bottom center">
             &nbsp;
         </li>-->

    </ul>

    <!-- END .slider -->
</div>


<div class="content-wrapper clearfix">

    <!-- BEGIN .content-wrapper-inner -->
    <div class="content-wrapper-inner clearfix">

        <!-- BEGIN .sidebar-left -->
        <div class="sidebar-left page-content">

            <!-- BEGIN .content-block -->
            <div class="content-block">

                <h3 class="block-title">Syllabus Download</h3>
                
                
                    <div class="">
                  <marquee direction="up" scrolldelay="50" truespeed="" scrollamount="1" behavior="sroll" onmouseover=this.stop() onmouseout=this.start()>

                      
                      <?php 
                      $i=0;
                      foreach($all_syllabus as $v_info)
                      {
                          $i+=1;
                      ?>
                        <h4 style="font-size: 12px;" ><a href="<?php echo base_url();?>welcome/syllabus"><?php echo $i;?>.<?php echo $v_info->syllabus_description;?></a></h4>
                        
                       <?php 
                      }
                       ?> 
                        
                        
<!--                        <h4 style="font-size: 12px;" ><a href="">2.&nbsp;Class XI (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">3.&nbsp;Class XI (Human Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">4.&nbsp;Class XII (Science) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">5.&nbsp;Class XII (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">6.&nbsp;Class XII (Human Studies) Department</a></h4>-->
                        </marquee> 
                    </div>
               
                    
                    <!--
<!--                <p>Find Your Class Syllabus</p>
                <script TYPE="text/javascript">
                    function popupform(myform, windowname) {
                        if (!window.focus)
                            return true;
                        window.open('', windowname, 'height=600,width=800,scrollbars=yes');
                        myform.target = windowname;
                        return true;
                    }
                </script>
                <form method="get" action="syllabusview.php" class="course-finder-form clearfix" onSubmit="popupform(this, 'join')">

                    <select name="keyword-type">
                        <option value="Course ID">course_id</option>
                    </select>
                    <input type="submit" value="Search" />

                </form>-->

                <!-- END .content-block -->	
            </div>
            <!-- BEGIN .content-block -->
            <div class="content-block">

                <h3 class="block-title">Class Routine</h3>
                <div class="">
                  <marquee direction="up" scrolldelay="50" truespeed="" scrollamount="1" behavior="sroll" onmouseover=this.stop() onmouseout=this.start()>

                        
                      
                     <?php 
                      $i=0;
                      foreach($class_routine as $v_info)
                      {
                          $i+=1;
                      ?>
                        <h4 style="font-size: 12px;" ><a href="<?php echo base_url();?>welcome/class_routine"><?php echo $i;?>.<?php echo $v_info->routine_description;?></a></h4>
                        
                       <?php 
                      }
                       ?> 
                        
                        
                        
                        
                        
<!--                        <h4 style="font-size: 12px;" ><a href="">2.&nbsp;Class XI (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">3.&nbsp;Class XI (Human Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">4.&nbsp;Class XII (Science) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">5.&nbsp;Class XII (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">6.&nbsp;Class XII (Human Studies) Department</a></h4>-->
                        </marquee> 
                    </div>
               

                <!-- END .content-block -->	
            </div>
            <!-- BEGIN .content-block -->
            <div class="content-block">

                <h3 class="block-title">Book List Download</h3>
                
                
                <div class="">
                  <marquee direction="up" scrolldelay="50" truespeed="" scrollamount="1" behavior="sroll" onmouseover=this.stop() onmouseout=this.start()>

                      <?php 
                      $i=0;
                      foreach($book_list as $v_info)
                      {
                          $i+=1;
                      ?>
                      <h4 style="font-size: 12px;" ><a href="<?php echo base_url();?>welcome/book_list"><?php echo $i;?>.<?php echo $v_info->book_description;?></a></h4>
                        
                       <?php 
                      }
                       ?> 
<!--                        <h4 style="font-size: 12px;" ><a href="">1.&nbsp;Class XI (Science) Department</a></h4>
                        <h4 style="font-size: 12px;" ><a href="">2.&nbsp;Class XI (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">3.&nbsp;Class XI (Human Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">4.&nbsp;Class XII (Science) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">5.&nbsp;Class XII (Business Studies) Department</a></h4>
                        <h4 style="font-weight: bold;font-size: 10px;" ><a href="">6.&nbsp;Class XII (Human Studies) Department</a></h4>-->
                        </marquee> 
                    </div>

                <!-- END .content-block -->	
            </div>
            
           </div>


        <!-- END .sidebar-left -->
    </div>	

    <!-- BEGIN .center-content -->
    <div class="center-content page-content">

        <!-- BEGIN .content-block -->
        <div class="content-block">
            <h3 class="block-title"><?php echo $welcome_message->welcome_short_message;?></h3>

            <p style=" text-align: justify;"><?php echo $welcome_message->welcome_long_message;?></p>
            <!--Principal Message-->

            <h3 class="block-title">প্রতিষ্ঠান প্রধানের বক্তব্য</h3>
            <img onerror="handleImgError(this)" src="<?php echo base_url().$principal_message->image;?>" width="120" height="130"/>
            <p style="text-align: justify;"><?php echo $principal_message->message_short_list;?></p>
            <p class="prin"><a href="<?php echo base_url();?>welcome/principal_message" class="readmore">Read More</a></p>

            <h3 class="block-title"><a href="<?php echo base_url();?>welcome/news_events.aspx">News &raquo;</a></h3>

            <!-- BEGIN .slider-blocks -->
            <div class="slider-blocks clearfix">

                <!-- BEGIN .slides -->
                <ul class="slides slide-loader2">

                    <!-- BEGIN .slides li -->
                    <li>

                        <!-- BEGIN .news-items -->
                        <ul class="news-items">

                           
                            
                            <marquee direction="up" scrolldelay="50" truespeed="" scrollamount="1" behavior="sroll" onmouseover=this.stop() onmouseout=this.start()>
                                <?php
                                $i = 0;
                                foreach ($all_info as $v_info) {
                                    $i+=1;
                                    ?>

                                <li class="">
                                        <div class="">
                                            <h4 style="font-weight: bold;font-size: 12px;" ><a href="<?php echo base_url();?>welcome/news_events_details/<?php echo $v_info->news_id;?>"><?php echo $i;?>.&nbsp;<?php echo $v_info->news_title;?></a></h4>
<!--                                            <p>Date: <?php echo date("F j, Y ", strtotime($v_info->news_date));?></p>-->
                                        </div>


                                    </li>
                                <?php
                                }
                                ?>

                             
                            </marquee>
                            
                            
                            
                            
                            
                            
                        </ul>

                        <!-- END .slides li -->
                    </li>

                    <!-- BEGIN .slides li -->
                    
                    <!-- END .slides -->
                </ul>

                <!-- END .slider-blocks -->
            </div>

            <!-- END .content-block -->	
        </div>

        <!-- BEGIN .content-block -->
        <!--<div class="content-block">-->

        <!--<h3 class="block-title"><a href="#">Video Tour &raquo;</a></h3>
        
        <div class="video-wrapper">
                <iframe style="width:100%;" height="215" src="http://player.vimeo.com/video/29521088?autoplay=0&amp;title=0&amp;byline=0&amp;portrait=0;"></iframe>
        </div>
        
        <p>Morbi quam arcu, sagittis sit amet gravida a, pretium id ligula. Sed pelle fermentum aliquam.</p>
        -->
        <!-- END .content-block -->	
        <!--</div>-->

        <!-- END .center-content -->
    </div>

    <!-- BEGIN .sidebar-right -->

    <?php echo $right_side_bar;?>

    <!-- END .content-wrapper-inner -->
</div>

<!-- END .content-wrapper -->
