<div class="page-header clearfix">

    <div class="page-header-inner clearfix">

        <div class="page-title">	
            <h2><?php echo $title; ?></h2>
            <div class="page-title-block"></div>
        </div>

        <div class="breadcrumbs">
            <p><a href="<?php echo base_url(); ?>welocme/index.aspx">Home</a> &#187;<?php echo $title; ?></p>
        </div>

    </div>

    <!-- END .page-header -->
</div>



<div class="content-wrapper page-content-wrapper clearfix">

    <!-- BEGIN .main-content -->
    <div class="main-content page-content">

        <!-- BEGIN .inner-content-wrapper -->
        <div class="inner-content-wrapper">
            
            <h2>Please Inform us if you have any objection</h2>
            <hr>
            <br>
           <form action="#" id="contactform" class="clearfix" method="post">

                <div class="field-row">
                    <label for="contact_name">Name <span>(required)</span></label>
                    <input type="text" name="contact_name" id="contact_name" class="text_input" value="" />
                </div>

                <div class="field-row">
                    <label for="email">Email <span>(required)</span></label>
                    <input type="text" name="email" id="email" class="text_input" value="" />		
                </div>

                <div class="field-row">
                    <label for="message_text">Message</label>
                    <textarea name="message" id="message_text" class="text_input" cols="60" rows="9"></textarea>
                </div>

                <input class="button1" type="submit" value="Send Email" id="submit" name="submit">

            </form>

            <!-- END .inner-content-wrapper -->
        </div>

        <!-- END .main-content -->
    </div>

    <!-- BEGIN .sidebar-right -->
    
    <?php echo $right_side_bar;?>

    <!-- END .content-wrapper -->
</div>
