-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2015 at 02:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_rgcbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_3rd_staff`
--

CREATE TABLE IF NOT EXISTS `tbl_3rd_staff` (
`id` int(4) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_3rd_staff`
--

INSERT INTO `tbl_3rd_staff` (`id`, `name`, `title`, `contact`, `image`) VALUES
(2, 'Korim mahmud', 'Accountant', '0155698745', 'images/3rd_staff/53.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_4th_staff`
--

CREATE TABLE IF NOT EXISTS `tbl_4th_staff` (
`id` int(4) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_academic_calender`
--

CREATE TABLE IF NOT EXISTS `tbl_academic_calender` (
`calender_id` int(4) NOT NULL,
  `calender_description` text,
  `calender_image` varchar(100) DEFAULT NULL,
  `calender_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_academic_calender`
--

INSERT INTO `tbl_academic_calender` (`calender_id`, `calender_description`, `calender_image`, `calender_date`) VALUES
(7, 'Academic Calender of 2015 ', 'file/academic_calender/application1.pdf', '2015-01-17 07:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(3) NOT NULL,
  `admin_name` varchar(100) DEFAULT NULL,
  `admin_email_address` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'dynamic', 'dynamicsoft@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admission_form`
--

CREATE TABLE IF NOT EXISTS `tbl_admission_form` (
`admission_id` int(4) NOT NULL,
  `admission_description` varchar(100) DEFAULT NULL,
  `admission_file` varchar(100) DEFAULT NULL,
  `admission_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admission_form`
--

INSERT INTO `tbl_admission_form` (`admission_id`, `admission_description`, `admission_file`, `admission_date`) VALUES
(2, 'Admission Information for HSC second year ', 'file/admission_info/08.pdf', '2015-01-17 12:40:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admission_information`
--

CREATE TABLE IF NOT EXISTS `tbl_admission_information` (
`admission_id` int(4) NOT NULL,
  `admission_description` varchar(100) DEFAULT NULL,
  `admission_file` varchar(100) DEFAULT NULL,
  `admission_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alternate_teacher`
--

CREATE TABLE IF NOT EXISTS `tbl_alternate_teacher` (
`teacher_id` int(4) NOT NULL,
  `teacher_description` text,
  `teacher_file` varchar(100) DEFAULT NULL,
  `teacher_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_alternate_teacher`
--

INSERT INTO `tbl_alternate_teacher` (`teacher_id`, `teacher_description`, `teacher_file`, `teacher_date`) VALUES
(2, 'alternate teacher', 'file/alternate_teacher/966-sec10.pdf', '2015-01-17 08:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_book_list`
--

CREATE TABLE IF NOT EXISTS `tbl_book_list` (
`book_id` int(4) NOT NULL,
  `book_description` text,
  `book_file` varchar(100) DEFAULT NULL,
  `book_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_book_list`
--

INSERT INTO `tbl_book_list` (`book_id`, `book_description`, `book_file`, `book_date`) VALUES
(2, 'book list', 'file/book_list/Book1.xls', '2015-01-18 09:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_citizen_charter`
--

CREATE TABLE IF NOT EXISTS `tbl_citizen_charter` (
`charter_id` int(3) NOT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_citizen_charter`
--

INSERT INTO `tbl_citizen_charter` (`charter_id`, `description`) VALUES
(1, 'Description');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE IF NOT EXISTS `tbl_class` (
`class_id` int(3) NOT NULL,
  `class_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_id`, `class_name`) VALUES
(1, 'একাদশ (বিজ্ঞান)'),
(2, 'একাদশ (ব্যবসায় শিক্ষা)'),
(3, 'একাদশ (মানবিক)'),
(4, 'দ্বাদশ (বিজ্ঞান)'),
(5, 'দ্বাদশ (ব্যবসায় শিক্ষা)'),
(6, 'দ্বাদশ (মানবিক)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class_routine`
--

CREATE TABLE IF NOT EXISTS `tbl_class_routine` (
`routine_id` int(4) NOT NULL,
  `routine_description` text,
  `routine_file` varchar(100) DEFAULT NULL,
  `routine_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_collage_history`
--

CREATE TABLE IF NOT EXISTS `tbl_collage_history` (
`history_id` int(3) NOT NULL,
  `history_title` varchar(100) DEFAULT NULL,
  `history_description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_collage_history`
--

INSERT INTO `tbl_collage_history` (`history_id`, `history_title`, `history_description`) VALUES
(1, 'কলেজের সংক্ষিপ্ত ইতিহাস', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_routine`
--

CREATE TABLE IF NOT EXISTS `tbl_exam_routine` (
`routine_id` int(4) NOT NULL,
  `routine_description` text,
  `routine_file` varchar(100) DEFAULT NULL,
  `routine_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_seat`
--

CREATE TABLE IF NOT EXISTS `tbl_exam_seat` (
`exam_id` int(4) NOT NULL,
  `exam_description` text,
  `exam_file` varchar(100) DEFAULT NULL,
  `exam_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_governing_body`
--

CREATE TABLE IF NOT EXISTS `tbl_governing_body` (
`id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_governing_body`
--

INSERT INTO `tbl_governing_body` (`id`, `name`, `title`, `contact`, `email`, `image`) VALUES
(1, 'Md. Hasibur Rahman', 'Lecturarer', '015569842273', 'Hasib@yahoo.com', 'images/gbody/11.jpg'),
(3, 'Md Israf Hossain', 'Assistant Professioir', '01987546213', 'email@email.com', 'images/gbody/1375076_668245376520800_1067707829_n.jpg'),
(4, 'Md. Manik khan ', 'senior lecturar', '01745369823', 'manik@yahoo.com', 'images/gbody/first.jpg'),
(5, 'Md. Sohanur Rahman', 'professor', '01647895213', 'sohan@yahoo.com', 'images/gbody/95.jpg'),
(6, 'Md Mustafiz', 'Lecturarer', '01556987456', 'mustafiz@yahoo.com', 'images/gbody/first1.jpg'),
(7, 'md. Asik Islam', 'senior lecturar', '01879654632', 'ashik@gmail.com', 'images/gbody/1375076_668245376520800_1067707829_n1.jpg'),
(8, 'Moinul Islam', 'professor', '01648796541', 'moinul@gmail.com', 'images/gbody/53.jpg'),
(9, 'Mithun khan', 'Lecturarer', '019658745896', 'khan@yahoo.com', 'images/gbody/79.jpg'),
(10, 'Kamrul hasan', 'Assistant Professor', '0119965864521', 'hasan@gmail.com', 'images/gbody/791.jpg'),
(11, 'Md. Taizul Islam', 'Lecturarer', '4558245824852', 'islam@email.com', 'images/gbody/1.jpg'),
(12, 'Md Masum', 'Lecturarer', '0178425696', 'nasum@gmail.com', 'images/gbody/12.jpg'),
(13, 'Md Kamal Akbar', 'Lecturarer', '213659', 'email@email.com', 'images/gbody/13.jpg'),
(14, 'Md Toibur Rahman', 'Assistant Professor', '213654', 'email@email.com', 'images/gbody/14.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
`news_id` int(4) NOT NULL,
  `news_title` varchar(100) DEFAULT NULL,
  `news_short_description` text,
  `news_long_description` text,
  `news_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `news_title`, `news_short_description`, `news_long_description`, `news_date`) VALUES
(1, ' বজ্রপাত নিয়ে মজার কথা', 'বৃষ্টির দিন, মন চাচ্ছেই না আজ স্কুলে যাই। জানালার পাশে বসে বসে বৃষ্টি দেখে দিন পার করে দেওয়াতেই যেন সব আনন্দ! দিনের বেলা হলেও চারদিক কালো মেঘে অন্ধকার হয়ে আছে। এর মধ্যেই হঠাৎ প্রচণ্ড আলোর ঝলকানিতে এক মুহূর্তের জন্য চারদিক আলোকিত হয়ে গেল।.............', 'বজ্রপাত হল বিদ্যুতের বিস্ফোরণ, যা অনেক শক্তিশালী। ঝড়বৃষ্টির সময় চোখের পলকে বজ্রপাত হতে পারে।\r\n\r\n•          তোমরা হয়ত জান, বৈদ্যুতিক প্রবাহের ধনাত্মক ও ঋণাত্মক আয়ন বা চার্জ আছে। দুটো মিলে একটি পরিবেশে একটি চার্জ নিরপেক্ষ অবস্থা তৈরি হয়। এখন পরিবেশে যদি কোনো একটি আয়নের সংখ্যা তবে বজ্রপাত ঘটে। এভাবে পরিবেশ আবার চার্জ নিরপেক্ষ হয়ে যায়।\r\n\r\n•          মেঘের ভেতর বৃষ্টি আর পানির চলাচলের ফলে তৈরি হয় বৈদ্যুতিক চার্জ। এ চার্জ আবার দুই রকম- ধনাত্মক আর ঋণাত্মক। প্রোটনের থাকে ধনাত্মক চার্জ, আর ইলেকট্রনের থাকে ঋণাত্মক। মেঘের নিচের দিকে থাকে ইলেকট্রন, আর উপরের দিকে প্রোটন।\r\n\r\n•          পরস্পর বিপরীতধর্মী হওয়ায় প্রোটন ও ইলেকট্রন একে অপরকে আকর্ষণ করে।\r\n\r\n•          বজ্রপাত মেঘের ভিতরেও হতে পারে, একাধিক মেঘের মধ্যেও হতে পারে, এমনকি মেঘ থেকে মাটিতেও হতে পারে।\r\n\r\n•          যত বজ্রপাত হয়, তার প্রায় এক চতুর্থাংশই হয় মেঘ থেকে মাটিতে।\r\n\r\n•          বজ্রপাতের সময় মেঘের যে ইলেকট্রন, সেগুলো যেকোনো ধনাত্মক চার্জ খুঁজতে থাকে। আশপাশের সবচেয়ে কাছে যাকে পায় সেদিকেই ছুটে যায় বিজলি! সেটা অনেক সময় একটি গাছের ওপর পড়ে, কখনও বা লম্বা কোনো ভবনের ওপর, আর ভাগ্য খারাপ হলে সরাসরি কোনো মানুষের ওপর।\r\n\r\n•          প্রতি বছর হাজার হাজার মানুষ বজ্রপাতের শিকার হয়।\r\n\r\n•          বিজলি সরাসরি এসে গায়ে পড়লে ফলাফল ভয়ংকর হতে পারে।\r\n\r\n•          উঁচু ভবনের ওপরে অনেক সময় বজ্রপাত থেকে রক্ষা পাওয়ার জন্য বিদ্যুৎ পরিবাহী ধাতব লম্বা দণ্ড ব্যবহার করা হয়। এগুলো মাটির সঙ্গে যুক্ত থাকে। তাই ভবনের ছাদে বাজ পড়লেও, এই ধাতুগুলো তা নিরাপদে মাটিতে পৌঁছে দেয়।\r\n\r\n•          বেশিরভাগ বজ্রপাত সাগরের ওপরেই হয়, এদের মধ্যে প্রায় ৭০ শতাংশই হয় ক্রান্তীয় অঞ্চলে।\r\n\r\n•          প্রতি সেকেন্ডেই পৃথিবীর কোথাও না কোথাও বজ্রপাত হচ্ছে।\r\n\r\n•          বজ্রপাত সাধারণত মাত্র ১ থেকে ২ মাইক্রো সেকেন্ড স্থায়ী হয়, এজন্যই তোমার মনে হয় আলোর ঝলকানি বোধহয় হয়েই মিলিয়ে গেল!\r\n\r\n•          বিজলির গড় তাপমাত্রা প্রায় ২০০০০ ডিগ্রি সেলসিয়াস (৩৬০০০ ডিগ্রি ফারেনহাইট)\r\n\r\n•          যেসব মেঘ অনেক ঘন আর লম্বা, সাধারণত সেগুলোতেই বজ্রপাত হয়।', '2015-01-13 08:02:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notice`
--

CREATE TABLE IF NOT EXISTS `tbl_notice` (
`notice_id` int(4) NOT NULL,
  `notice_title` varchar(100) DEFAULT NULL,
  `notice_short_description` text,
  `notice_long_description` text,
  `notice_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_notice`
--

INSERT INTO `tbl_notice` (`notice_id`, `notice_title`, `notice_short_description`, `notice_long_description`, `notice_date`) VALUES
(1, 'পড়তে চাইলে বস্ত্র প্রকৌশল', 'এইচএসসি পাস করা অনেক শিক্ষার্থী এখনো ভর্তিযুদ্ধের ময়দানে। এদিকে বেশির ভাগ বিশ্ববিদ্যালয়ের ভর্তি পরীক্ষাই শেষের পথে। তাই হন্যে হয়ে খুঁজছেন আর কোথায় বাঁধা যায় নিজের উচ্চশিক্ষার স্বপ্নটাকে। তাঁদের জন্য সুখবর হতে পারে বস্ত্র প্রকৌশলী...............\r\n', 'এইচএসসি পাস করা অনেক শিক্ষার্থী এখনো ভর্তিযুদ্ধের ময়দানে। এদিকে বেশির ভাগ বিশ্ববিদ্যালয়ের ভর্তি পরীক্ষাই শেষের পথে। তাই হন্যে হয়ে খুঁজছেন আর কোথায় বাঁধা যায় নিজের উচ্চশিক্ষার স্বপ্নটাকে। তাঁদের জন্য সুখবর হতে পারে বস্ত্র প্রকৌশলী হওয়ার স্বপ্ন।\r\nঢাকা বিশ্ববিদ্যালয়ের অধিভুক্ত এবং বাংলাদেশ সরকারের পাবলিক প্রাইভেট পার্টনারশিপের (পিপিপি) আওতায় বাংলাদেশ টেক্সটাইল মিলস অ্যাসোসিয়েশন (বিটিএমএ) কর্তৃক পরিচালিত হচ্ছে জাতীয় বস্ত্র প্রকৌশল ও গবেষণা ইনস্টিটিউট (নিটার)। ২০১০-২০১১ শিক্ষাবর্ষে যাত্রা শুরু করা এই প্রতিষ্ঠানে থাকছে চার বছরমেয়াদি বিএসসি ইন টেক্সটাইল ইঞ্জিনিয়ারিং কোর্স। ইয়ার্ন ম্যানুফেকচারিং, ফ্যাব্রিক ম্যানুফেকচারিং, ওয়েট প্রসেসিং ও অ্যাপারেল ম্যানুফেকচারিং বিষয়ে অধ্যয়নের সুযোগ পান শিক্ষার্থীরা। বর্তমানে অধ্যয়নরত নিটারের মোট ৪২০ জন শিক্ষার্থীর মধ্যে রয়েছে ৩৮০ জন ছাত্র ও ৪০ জন ছাত্রী। রাজধানীর অদূরে সাভারের নয়ারহাটে ১৩.০৬ একর জায়গাজুড়ে নিটারের নিজস্ব ক্যাম্পাস। একাডেমিক ও প্রশাসনিক ভবনের পাশাপাশি ক্যাম্পাসের ভিতরেই আছে দুটি ছাত্রাবাস ও একটি ছাত্রীনিবাস।', '2015-01-12 18:00:00'),
(2, 'তথ্যপ্রযুক্তি বিভাগের এক বছর', 'গত এক বছরে বাংলাদেশের তথ্যপ্রযুক্তি খাতের অগ্রগতি সম্পর্কে জানাতে আজ বিসিসি অডিটোরিয়ামে এক সংবাদ সম্মেলনের আয়োজন করে তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ। সম্মেলনে প্রতিমন্ত্রী জুনাইদ আহমেদ গত এক বছরে তথ্যপ্রযুক্তি বিভাগের বিভিন্ন কার্যক্রম সম্পর্কে.....', 'গত এক বছরে বাংলাদেশের তথ্যপ্রযুক্তি খাতের অগ্রগতি সম্পর্কে জানাতে আজ বিসিসি অডিটোরিয়ামে এক সংবাদ সম্মেলনের আয়োজন করে তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ। সম্মেলনে প্রতিমন্ত্রী জুনাইদ আহমেদ গত এক বছরে তথ্যপ্রযুক্তি বিভাগের বিভিন্ন কার্যক্রম সম্পর্কে জানান।\r\nপ্রতিমন্ত্রী বলেন, গত এক বছরে বেসরকারি উদ্যোক্তাদের জন্য ব্যবসাবান্ধব পরিবেশ তৈরিতে কাজ করেছেন তাঁরা। দেশের প্রযুক্তি খাতের সব ব্যবসায়ী সংগঠনকে কার্যক্রমের সঙ্গে যুক্ত করা হয়েছে। এ ছাড়াও ২০০৯ সালের আইসিটি নীতিমালাকে যুগোপযোগী করতে সংশোধিত আইসিটি পলিসি-২০১৫-এর চূড়ান্ত খসড়া প্রণয়ন করা হয়েছে যা মন্ত্রিসভায় অনুমোদনের অপেক্ষায় রয়েছে।\r\nজুনাইদ আহমেদ জানান, গত এক বছরে ই-সার্ভিসসমূহকে আইনি কাঠামো প্রদানের লক্ষ্যে ই-সার্ভিস আইন প্রণয়ন করা হয়েছে। হাইটেক পার্ক কর্তৃপক্ষ আইন-২০১০ সংশোধন করা হয়েছে। সাইবার অপরাধ দমনে আমরা সাইবার নিরাপত্তা নীতিমালা প্রণয়ন করা হয়েছে। তথ্যপ্রযুক্তি খাতে গবেষণামূলক কার্যক্রমে উৎসাহ দিতে আইসিটি ফেলোশিপ চালু করা হয়েছে। তথ্যপ্রযুক্তি খাতে দক্ষ জনবল তৈরিতে বিভিন্ন প্রশিক্ষণ কর্মসূচি চালু করা হয়েছে। এর মধ্যে রয়েছে আইটি/আইটিএস, লিভারেজিং আইটিসি ফর গ্রোথ, লার্নিং অ্যান্ড আর্নিং, বাড়ি বসে বড়লোকের মতো নানা কর্মসূচি।', '2015-01-13 06:36:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_photo_gallary`
--

CREATE TABLE IF NOT EXISTS `tbl_photo_gallary` (
`image_id` int(4) NOT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_photo_gallary`
--

INSERT INTO `tbl_photo_gallary` (`image_id`, `image`) VALUES
(2, 'images/photo_gallary/96.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_created`
--

CREATE TABLE IF NOT EXISTS `tbl_post_created` (
`post_id` int(4) NOT NULL,
  `post_description` text,
  `post_file` varchar(100) DEFAULT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_post_created`
--

INSERT INTO `tbl_post_created` (`post_id`, `post_description`, `post_file`, `post_date`) VALUES
(2, 'post created 2', 'file/post/created/Book1.xls', '2015-01-18 06:23:05'),
(4, 'post vacant', 'file/post/created/Book1.xlsx', '2015-01-18 06:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_vacant`
--

CREATE TABLE IF NOT EXISTS `tbl_post_vacant` (
`post_id` int(4) NOT NULL,
  `post_description` text,
  `post_file` varchar(100) DEFAULT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_principal_message`
--

CREATE TABLE IF NOT EXISTS `tbl_principal_message` (
`message_id` int(3) NOT NULL,
  `principal_name` varchar(100) DEFAULT NULL,
  `message_short_list` text,
  `message_long_list` text,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_principal_message`
--

INSERT INTO `tbl_principal_message` (`message_id`, `principal_name`, `message_short_list`, `message_long_list`, `image`) VALUES
(1, 'MD. Nurul Islam', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। ', ' ১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।', 'images/principal/11.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_result`
--

CREATE TABLE IF NOT EXISTS `tbl_result` (
`result_id` int(4) NOT NULL,
  `result_description` text,
  `result_file` varchar(100) DEFAULT NULL,
  `result_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
`slider_id` int(3) NOT NULL,
  `slider_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_image`) VALUES
(7, 'images/slider/1.jpg'),
(8, 'images/slider/2.jpg'),
(9, 'images/slider/4.jpg'),
(10, 'images/slider/5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE IF NOT EXISTS `tbl_staff` (
`id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`id`, `name`, `title`, `contact`, `email`, `image`) VALUES
(1, 'Shoikot Islam', 'Accountant', '01236548798', 'shoikot@ymail.com', 'images/staff/77.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE IF NOT EXISTS `tbl_student` (
`s_id` int(6) NOT NULL,
  `s_name` varchar(50) DEFAULT NULL,
  `s_year` varchar(20) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `s_roll` varchar(30) DEFAULT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `m_name` varchar(50) DEFAULT NULL,
  `s_religion` varchar(20) DEFAULT NULL,
  `s_sex` varchar(20) DEFAULT NULL,
  `s_birth_date` varchar(30) DEFAULT NULL,
  `s_contact` varchar(40) DEFAULT NULL,
  `present_address` text,
  `parmanent_address` text,
  `image` varchar(100) DEFAULT NULL,
  `s_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`s_id`, `s_name`, `s_year`, `class_id`, `s_roll`, `f_name`, `m_name`, `s_religion`, `s_sex`, `s_birth_date`, `s_contact`, `present_address`, `parmanent_address`, `image`, `s_date`) VALUES
(2, 'Mustafizur rahman', '2015', 1, '1', 'Amir Hossain', 'Monoara Begum', 'islam', 'male', '13/03/1989', '01556984227', 'dhaka', 'dhaka', 'images/student/College_picture2.JPG', '2015-01-21 07:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_syllabus`
--

CREATE TABLE IF NOT EXISTS `tbl_syllabus` (
`syllabus_id` int(4) NOT NULL,
  `syllabus_description` text,
  `syllabus_file` varchar(100) DEFAULT NULL,
  `syllabus_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_syllabus`
--

INSERT INTO `tbl_syllabus` (`syllabus_id`, `syllabus_description`, `syllabus_file`, `syllabus_date`) VALUES
(2, 'syllabus', 'file/syllabus/application1.pdf', '2015-01-18 09:22:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher`
--

CREATE TABLE IF NOT EXISTS `tbl_teacher` (
`id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `join_date` varchar(50) NOT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `blood_group` varchar(30) NOT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_teacher`
--

INSERT INTO `tbl_teacher` (`id`, `name`, `title`, `subject`, `join_date`, `contact`, `email`, `blood_group`, `image`) VALUES
(2, 'Khandaker Monjur Morshed', 'Associate Professor, Vice Principle', 'Islamic History & Culture', '03/04/2014', '01712549326', 'ramgonj_college@yahoo.com', 'A+', 'images/teacher/Morshed_sir_photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
`welcome_id` int(3) NOT NULL,
  `welcome_short_message` text NOT NULL,
  `welcome_long_message` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`welcome_id`, `welcome_short_message`, `welcome_long_message`) VALUES
(1, 'স্বাগতম রামগঞ্জ সরকারী কলেজ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_3rd_staff`
--
ALTER TABLE `tbl_3rd_staff`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_4th_staff`
--
ALTER TABLE `tbl_4th_staff`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_academic_calender`
--
ALTER TABLE `tbl_academic_calender`
 ADD PRIMARY KEY (`calender_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_admission_form`
--
ALTER TABLE `tbl_admission_form`
 ADD PRIMARY KEY (`admission_id`);

--
-- Indexes for table `tbl_admission_information`
--
ALTER TABLE `tbl_admission_information`
 ADD PRIMARY KEY (`admission_id`);

--
-- Indexes for table `tbl_alternate_teacher`
--
ALTER TABLE `tbl_alternate_teacher`
 ADD PRIMARY KEY (`teacher_id`);

--
-- Indexes for table `tbl_book_list`
--
ALTER TABLE `tbl_book_list`
 ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `tbl_citizen_charter`
--
ALTER TABLE `tbl_citizen_charter`
 ADD PRIMARY KEY (`charter_id`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
 ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_class_routine`
--
ALTER TABLE `tbl_class_routine`
 ADD PRIMARY KEY (`routine_id`);

--
-- Indexes for table `tbl_collage_history`
--
ALTER TABLE `tbl_collage_history`
 ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `tbl_exam_routine`
--
ALTER TABLE `tbl_exam_routine`
 ADD PRIMARY KEY (`routine_id`);

--
-- Indexes for table `tbl_exam_seat`
--
ALTER TABLE `tbl_exam_seat`
 ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `tbl_governing_body`
--
ALTER TABLE `tbl_governing_body`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
 ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
 ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `tbl_photo_gallary`
--
ALTER TABLE `tbl_photo_gallary`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `tbl_post_created`
--
ALTER TABLE `tbl_post_created`
 ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tbl_post_vacant`
--
ALTER TABLE `tbl_post_vacant`
 ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tbl_principal_message`
--
ALTER TABLE `tbl_principal_message`
 ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_result`
--
ALTER TABLE `tbl_result`
 ADD PRIMARY KEY (`result_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
 ADD PRIMARY KEY (`s_id`), ADD KEY `fk_class_id` (`class_id`);

--
-- Indexes for table `tbl_syllabus`
--
ALTER TABLE `tbl_syllabus`
 ADD PRIMARY KEY (`syllabus_id`);

--
-- Indexes for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
 ADD PRIMARY KEY (`welcome_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_3rd_staff`
--
ALTER TABLE `tbl_3rd_staff`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_4th_staff`
--
ALTER TABLE `tbl_4th_staff`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_academic_calender`
--
ALTER TABLE `tbl_academic_calender`
MODIFY `calender_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_admission_form`
--
ALTER TABLE `tbl_admission_form`
MODIFY `admission_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_admission_information`
--
ALTER TABLE `tbl_admission_information`
MODIFY `admission_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_alternate_teacher`
--
ALTER TABLE `tbl_alternate_teacher`
MODIFY `teacher_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_book_list`
--
ALTER TABLE `tbl_book_list`
MODIFY `book_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_citizen_charter`
--
ALTER TABLE `tbl_citizen_charter`
MODIFY `charter_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
MODIFY `class_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_class_routine`
--
ALTER TABLE `tbl_class_routine`
MODIFY `routine_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_collage_history`
--
ALTER TABLE `tbl_collage_history`
MODIFY `history_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_exam_routine`
--
ALTER TABLE `tbl_exam_routine`
MODIFY `routine_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_exam_seat`
--
ALTER TABLE `tbl_exam_seat`
MODIFY `exam_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_governing_body`
--
ALTER TABLE `tbl_governing_body`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
MODIFY `news_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
MODIFY `notice_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_photo_gallary`
--
ALTER TABLE `tbl_photo_gallary`
MODIFY `image_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_post_created`
--
ALTER TABLE `tbl_post_created`
MODIFY `post_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_post_vacant`
--
ALTER TABLE `tbl_post_vacant`
MODIFY `post_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_principal_message`
--
ALTER TABLE `tbl_principal_message`
MODIFY `message_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_result`
--
ALTER TABLE `tbl_result`
MODIFY `result_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
MODIFY `slider_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
MODIFY `s_id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_syllabus`
--
ALTER TABLE `tbl_syllabus`
MODIFY `syllabus_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
MODIFY `welcome_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_student`
--
ALTER TABLE `tbl_student`
ADD CONSTRAINT `tbl_student_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `tbl_class` (`class_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
