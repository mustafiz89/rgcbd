-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2015 at 01:29 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_rgcbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int(3) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(100) DEFAULT NULL,
  `admin_email_address` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'dynamic', 'dynamicsoft@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_governing_body`
--

CREATE TABLE IF NOT EXISTS `tbl_governing_body` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_governing_body`
--

INSERT INTO `tbl_governing_body` (`id`, `name`, `title`, `contact`, `email`, `image`) VALUES
(1, 'Md. Hasibur Rahman', 'Lecturarer', '01556984227', 'Hasib@yahoo.com', 'images/gbody/11.jpg'),
(3, 'Md Israf Hossain', 'Assistant Professioir', '01987546213', 'email@email.com', 'images/gbody/1375076_668245376520800_1067707829_n.jpg'),
(4, 'Md. Manik khan ', 'senior lecturar', '01745369823', 'manik@yahoo.com', 'images/gbody/first.jpg'),
(5, 'Md. Sohanur Rahman', 'professor', '01647895213', 'sohan@yahoo.com', 'images/gbody/95.jpg'),
(6, 'Md Mustafiz', 'Lecturarer', '01556987456', 'mustafiz@yahoo.com', 'images/gbody/first1.jpg'),
(7, 'md. Asik Islam', 'senior lecturar', '01879654632', 'ashik@gmail.com', 'images/gbody/1375076_668245376520800_1067707829_n1.jpg'),
(8, 'Moinul Islam', 'professor', '01648796541', 'moinul@gmail.com', 'images/gbody/53.jpg'),
(9, 'Mithun khan', 'Lecturarer', '019658745896', 'khan@yahoo.com', 'images/gbody/79.jpg'),
(10, 'Kamrul hasan', 'Assistant Professor', '0119965864521', 'hasan@gmail.com', 'images/gbody/791.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `news_id` int(4) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(100) DEFAULT NULL,
  `news_short_description` text,
  `news_long_description` text,
  `news_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `news_title`, `news_short_description`, `news_long_description`, `news_date`) VALUES
(1, 'স্বাগতম রামগঞ্জ সরকারী কলেজ  স্বাগতম রামগঞ্জ সরকারী কলেজ স্বাগতম রামগঞ্জ সরকারী কলেজ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে।', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।', '2015-01-14 00:08:34'),
(2, 'স্বাগতম রামগঞ্জ সরকারী কলেজ  স্বাগতম রামগঞ্জ সরকারী কলেজ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে।', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।', '2015-01-14 00:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notice`
--

CREATE TABLE IF NOT EXISTS `tbl_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(100) DEFAULT NULL,
  `notice_short_description` text,
  `notice_long_description` text,
  `notice_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_notice`
--

INSERT INTO `tbl_notice` (`notice_id`, `notice_title`, `notice_short_description`, `notice_long_description`, `notice_date`) VALUES
(1, 'স্বাগতম রামগঞ্জ সরকারী কলেজ  স্বাগতম রামগঞ্জ সরকারী কলেজ  ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে।', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।', '2015-01-14 00:04:12'),
(2, 'স্বাগতম রামগঞ্জ সরকারী কলেজ  স্বাগতম রামগঞ্জ সরকারী কলেজ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে।', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।', '2015-01-14 00:05:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_principal_message`
--

CREATE TABLE IF NOT EXISTS `tbl_principal_message` (
  `message_id` int(3) NOT NULL AUTO_INCREMENT,
  `principal_name` varchar(100) DEFAULT NULL,
  `message_short_list` varchar(500) DEFAULT NULL,
  `message_long_list` varchar(3000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_principal_message`
--

INSERT INTO `tbl_principal_message` (`message_id`, `principal_name`, `message_short_list`, `message_long_list`, `image`) VALUES
(1, 'MD. Nurul Islam', 'Our school environment is a top priority for the administration and staff at EES. We work hard to maintain a safe and orderly workplace for students and teachers.  Our class rooms are clean and well lit so students', ' Our school environment is a top priority for the administration and staff at EES. We work hard to maintain a safe and orderly workplace for students and teachers.  Our class rooms are clean and well lit so students can pass from one area to another with little fear of student or facility issues during the school day.  We have a regular schedule for emergency drills so students and parents know we are trying to better prepare ourselves in case of an unexpected break in our routines for the day/week because of some natural event or man made incident.  Our students are expected to dress appropriately and behave respectfully at all times. One of our major goals is to eliminate distractions to our students’ daily educational lessons.  We make every attempt to be consistent with all students and treat everyone in a fair and just fashion.\r\n\r\n  All EES students are strongly encouraged to attend classes every day and also to take advantage of the challenging courses and the educational interventions provided throughout the year.  The staff has developed and delivers a curriculum that will help prepare the communities young adults for their world beyond high school.  We offer a variety of Edexcel  and Cambridge High School courses.   We have the typical course offerings found in most comprehensive high schools in the disciplines of Mathematics, Physical and Natural Sciences, Language Arts/Speech and the Social Sciences.', 'images/principal/1375076_668245376520800_1067707829_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `slider_id` int(3) NOT NULL AUTO_INCREMENT,
  `slider_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE IF NOT EXISTS `tbl_staff` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`id`, `name`, `title`, `contact`, `email`, `image`) VALUES
(1, 'Shoikot Islam', 'Accountant', '01236548798', 'shoikot@ymail.com', 'images/staff/77.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher`
--

CREATE TABLE IF NOT EXISTS `tbl_teacher` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_teacher`
--

INSERT INTO `tbl_teacher` (`id`, `name`, `title`, `contact`, `email`, `image`) VALUES
(1, 'Aminur Khan', 'Assistant Professor', '015569874563', 'aminur@yahoo.com', 'images/teacher/68.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
  `welcome_id` int(3) NOT NULL AUTO_INCREMENT,
  `welcome_short_message` varchar(200) NOT NULL,
  `welcome_long_message` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`welcome_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`welcome_id`, `welcome_short_message`, `welcome_long_message`) VALUES
(1, 'স্বাগতম রামগঞ্জ সরকারী কলেজ', '১৯৮৫ সনের সেপ্টেম্বর মাসে তৎকালীন রাষ্ট্রপতি হুসেন মুহাম্মদ এরশাদ কলেজটিকে জাতীয়করণ করেন। ১৯৮৬ সনের ১ ফেব্রুয়ারী থেকে কলেজটির সরকারি কার্যক্রম শুরু হয়। রামগঞ্জ সরকারি কলেজ নামে কলেজটির যাত্রা শুরু হল। ক্রমান্বয়ে কলেজটি বর্তমান অবস্থায় এসে পৌঁছেছে। নির্মিত হয়েছে ত্রিতল একাডেমিক ভবন, দুটি দ্বিতল একাডেমিক ভবন, নতুন বিজ্ঞান ভবন, দ্বিতল প্রশাসনিক ভবন, একটি ইন্টারনেট সংযোগসহ অত্যাধুনিক কম্পিউটার ল্যাব, সমৃদ্ধ গ্রন্থাগার, আধুনিক বিজ্ঞানাগার, আকর্ষণীয় বোটানিক্যাল গার্ডেন, বি.এন.সি.সি যুব রেড ক্রিসেন্ট, রোভার স্কাউটস রয়েছে। ৯.৭৩ একর ভূমির উপর কলেজটি প্রতিষ্ঠিত। বর্তমানে বোর্ড ও বিশ্ববিদ্যালয়ে পাবলিক পরীক্ষায় এ কলেজ ঈষণীয় সাফল্য অর্জন করছে। এ কলেজ থেকে পাশ করা ছাত্র-ছাত্রীরা দেশ ও বিদেশের বিভিন্ন বিশ্ববিদ্যালয়ের শিক্ষকতা, সরকারি উচ্চপদস্থ কর্মকর্তা এবং নামকরা বিভিন্ন প্রতিষ্ঠানে উচ্চ পদে কর্মরত আছেন। আশা করা যায় এই সরকারি কলেজটি ক্রমান্বয়ে উন্নতির দিকে এগিয়ে যাবে।');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
